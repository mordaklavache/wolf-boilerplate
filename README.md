﻿# RayCasting in WASM Rust - POC

---

**This small 'web page' is written in the Rust programming language applied to WebAssembly technology.**

**YOU CAN SEE [A DEMONSTRATION HERE](https://mordaklava.ch) !**

## The 'Wolf3d' - An Old 42's school project

Here it is about porting a small 42 school project in Paris dealing with RayCasting initially written in C to WebAssembly technologies with Rust.

### Here are some screenshots

After the loading screen.

![ALT](README/intro.png)
### Turn off the light

With the **L** key, you can turn off the light.

![ALT](README/light_off.png)
### Mutithreading

I used here multithread web technologies thanks to Workers. The **Rayon** crate which allows to easily parallelize several operations between threads is also used here.

![ALT](README/mutithreads.png)
### Nostalgia mode with Factor 4

In old school style.

![ALT](README/nostalgia.png)

## What is used here

### STD USAGE

Apart from the notable absence of std::fs and std::thread, most of the Rust std is usable for the web.

- ops::**Index**;
- marker::**Sync**;
- cell::**RefCell**;
- rc::**Rc**;
- cmp::**Ordering**;
- ptr::{**copy**, **slice_from_raw_parts**};
- sync::atomic::Ordering::**SeqCst**;
- f32::consts::**PI**;
- collections::**BTreeMap**;
- ffi::{**c_char**, **CString**, **OsStr**};
- io::{**Read**, **Result**};
- mem::{**size_of**, **transmute**};
- path::**Path**;
- panic::**set_hook**

### RUST CRATES

- **wasm-bindgen** = "0.2.87"
- **wasm-bindgen-futures** = "0.4.37"
- **futures-util** = "0.3.28"
- **js-sys** = "0.3.64"
- **wasm-logger** = "0.2.0"
- **rand** = {version = "0.7.3", features = ["wasm-bindgen"] }
- **log** = "0.4.6"
- **console_error_panic_hook** = { version = "0.1.7"}
- **rayon** = "1.7.0"
- **atomic** = "0.5.3"
- **png** = "0.17.9"

### WEB-SYS DOM FEATURES

The web-sys functionalities related to the DOM of Web browsers are here:

Which allow to interact with the DOM in a similar way to JavaScript.
The web_worker crate allows multithreading by spawning Workers running in parallel.
The rayon crate brings support for parallel iterators to easily parallelize operations.
So most of the Rust ecosystem can be leveraged thanks to WebAssembly!

```
[dependencies.web-sys]
version = "0.3.63"
features = [
  'Document',
  'Window',
  'ImageData',
  'CanvasRenderingContext2d',
  'HtmlCanvasElement',
  'EventTarget',
  'KeyboardEvent',
  'AudioContext',
  'AudioBuffer',
  'AudioBufferSourceNode',
  'AudioDestinationNode',
  'Response',
  'Performance',
  'PerformanceTiming',
  'DomTokenList',
  'Worker',
  'DedicatedWorkerGlobalScope',
  'MessageEvent',
  'ErrorEvent',
  'DeviceMotionEvent',  # Mobile devices
  'DeviceAcceleration', # Mobile devices
  'Screen',             # Mobile devices
  'ScreenOrientation',  # Mobile devices
  'OrientationType',    # Mobile devices
]
```

### JS_SYS USES FEATURES

The js_sys crate represents the pure JavaScript API

- Array
- ArrayBuffer
- Uint8ClampedArray
- Uint8Array
- WebAssembly
- Promise
- Date

If you want to learn more, I highly recommend [The wasm-bindgen guide](https://rustwasm.github.io/wasm-bindgen/).