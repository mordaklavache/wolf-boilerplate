//! OPT when resizing window, follow screen pixels for excellent display.  
//! OPT web-sys Keycode is maybe present. (must use code() instead!).  
//! OPT ReWrite Approach on lib to ensure all is always on calculus. (trash move_sprites.rs quick fix).  
//! OPT bigfix on dev mode. overflow render_sprites.rs l112 (maybe l116 too).  
#![deny(missing_docs)]
mod game;
mod graphics;
mod map;
mod pool;

use atomic::Atomic;
use js_sys::Uint8ClampedArray;
use rayon::ThreadPool;
use wasm_bindgen::prelude::*;
use wasm_boilerplate::*;
use web_sys::DedicatedWorkerGlobalScope;

use std::cell::RefCell;
use std::cmp::Ordering;
use std::ptr::slice_from_raw_parts;
use std::rc::Rc;
use std::sync::atomic::Ordering::SeqCst;

const DEFAULT_PLAYER_HEIGHT: f32 = 1.4;
const DEFAULT_PLAYER_ANGLE: f32 = 6. / 4. * std::f32::consts::PI;
const DEFAULT_WALL_HEIGHT: f32 = 3.0;
const DEFAULT_SPRITE_HEIGHT: f32 = 1.2;

/// Informations about the first person.
#[derive(Debug, Copy, Clone)]
pub struct Player {
    location: Coord<f32>,
    height: f32,
    angle: f32,
}

impl Default for Player {
    fn default() -> Self {
        Self {
            location: Coord::default(),
            height: DEFAULT_PLAYER_HEIGHT,
            angle: DEFAULT_PLAYER_ANGLE,
        }
    }
}

#[derive(Debug, Copy, Clone, Default, PartialEq, PartialOrd, Eq, Ord)]
#[repr(usize)]
enum WallKind {
    #[default]
    FourWall,
    FourWall2,
    Piggie,
    Aperture,
    Panic,
}

/// Floor type.
#[derive(Debug, Copy, Clone, Default, PartialEq, PartialOrd, Eq, Ord)]
#[repr(usize)]
pub enum FloorKind {
    /// flooring
    #[default]
    Parquet,
    /// beautifull carpet
    Carpet,
    /// juste stones
    Stone,
}

/// Information about a wall.
#[derive(Debug, Copy, Clone)]
pub struct Wall {
    kind: WallKind,
    height: f32,
}

impl Default for Wall {
    fn default() -> Self {
        Self {
            kind: WallKind::default(),
            height: DEFAULT_WALL_HEIGHT,
        }
    }
}

/// Describes the element type, wall or floor.
#[derive(Debug, Copy, Clone)]
pub enum Elem {
    /// Wall type
    Wall(Wall),
    /// Floor type
    Floor(FloorKind),
}

#[derive(Debug, Copy, Clone, Default, PartialEq, PartialOrd, Eq, Ord)]
#[repr(usize)]
enum SpriteKind {
    #[default]
    Pig,
    Dog,
    Sadirak,
}

/// This structure contains information related to sprites.
#[derive(Debug, Copy, Clone)]
pub struct Sprite {
    map_location: Coord<usize>, // Unsigned integer location on map
    kind: SpriteKind,
    height: f32,

    // Sprite mouvements
    origin_location: Coord<f32>,
    current_location: Coord<f32>,
    target_location: Coord<f32>,
    last_time: Option<f64>,

    // Internal for rendering
    dist: f32,
    angle0_x: f32,
}

impl Default for Sprite {
    fn default() -> Self {
        Self {
            map_location: Coord::default(),
            origin_location: Coord::default(),
            current_location: Coord::default(),
            target_location: Coord::default(),
            last_time: None,
            kind: SpriteKind::default(),
            height: DEFAULT_SPRITE_HEIGHT,

            dist: 0.0,
            angle0_x: 0.0,
        }
    }
}

impl PartialEq for Sprite {
    fn eq(&self, other: &Self) -> bool {
        self.dist == other.dist
    }
}

impl PartialOrd for Sprite {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.dist.partial_cmp(&other.dist)
    }
}

#[derive(Debug, Copy, Clone, Default, Eq, PartialEq)]
enum Key {
    Pressed,
    #[default]
    Released,
}

#[derive(Debug, Copy, Clone, Default)]
struct KeyStatus {
    state: Key,
    executed: bool,
}

impl KeyStatus {
    fn must_be_executed(&self) -> bool {
        self.state == Key::Pressed && !self.executed
    }
    fn mark_as_executed(&mut self) {
        self.executed = true;
    }
}

impl wasm_boilerplate::Keyboard for KeyStatus {
    fn keydown(&mut self) {
        self.state = Key::Pressed;
    }
    fn keyup(&mut self) {
        self.state = Key::Released;
        self.executed = false;
    }
}

#[derive(Debug, Copy, Clone)]
#[repr(usize)]
enum KeyCode {
    Left = 37,
    Right = 39,
    Down = 40,
    Up = 38,
    A = 65,
    D = 68,
    W = 87,
    S = 83,
    I = 73,
    L = 76,
    M = 77,
    N = 78,
    T = 84,
}

/// Handles keyboard input and command mapping.
/// The commands will be passed to the main worker.
#[derive(Debug, Default)]
pub struct Commands {
    toggle_linear_interpolation: bool,
    toggle_light: bool,
    toggle_minimap: bool,
    toggle_nostalgia: bool,
    left: bool,
    right: bool,
    up: bool,
    down: bool,
}

/// Is the general context, it will be passed via a pointer to the main worker
pub struct Context {
    thread_pool: Option<ThreadPool>,
    map: Vec<Vec<Elem>>,
    sprites: Vec<Sprite>,
    player: Player,
    graphics: graphics::Graphics,
}

#[derive(Debug, Copy, Clone)]
#[repr(usize)]
enum State {
    Rendering,
    NewFrameAvailable,
}

struct SharedFrameBuffer {
    frame_buffer: Vec<Rgba>,
    state: Atomic<State>,
}

static mut SFB: Option<SharedFrameBuffer> = None;

#[wasm_bindgen(start)]
async fn run() -> Result<(), JsValue> {
    let mut ctx = wasm_boilerplate::GraphicContext::new()?;
    let win = ctx.get_dimensions();
    log::info!("Dimension X: {}, Y: {}", win.x, win.y);

    let navigator = web_sys::window().unwrap().navigator();
    let threads = navigator.hardware_concurrency() as usize;
    log::info!("{} threads available!", threads as usize);

    let map = include_str!("../ressources/maps/mapZ.map");
    let (map, player, sprites) = map::parse(map);

    let graphics = graphics::Graphics::new(
        win,
        player,
        match cfg!(debug_assertions) {
            true => Some(graphics::NostalgiaFactor(4)),
            false => None,
        },
        &mut ctx,
    );

    let song = include_bytes!("../ressources/musics/kiwi.mp3");
    wasm_boilerplate::song::song(song)?;

    let key_register = Rc::new(RefCell::new(Register {
        keyboard: [KeyStatus::default(); 256],
        acceleration_including_gravity: MotionCoord::default(),
        acceleration: MotionCoord::default(),
        acceleration_sum: MotionCoord::default(),
        orientation: wasm_boilerplate::get_screen_orientation_type(),
    }));
    wasm_boilerplate::set_user_events(key_register.clone());

    let mut frames_count = 0;
    let mut fps = 0.;
    let mut start_time: Option<f64> = None;

    let last_time = Rc::new(RefCell::new(0.));
    let last_time_clone = last_time.clone();

    // Mutithread initialisation
    let (primary_worker_pool, mut sc_context) = if threads >= 4 {
        let parralel_threads = threads - 2;
        // Auxiliary Workers.
        let worker_pool = pool::WorkerPool::new(parralel_threads, Box::new(())).unwrap();
        let thread_pool = rayon::ThreadPoolBuilder::new()
            .num_threads(parralel_threads)
            .spawn_handler(|thread| {
                worker_pool.run(|_context| thread.run()).unwrap();
                Ok(())
            })
            .build()
            .unwrap();
        // main Worker
        (
            Some(
                pool::WorkerPool::new(
                    1,
                    Box::new(Context {
                        thread_pool: Some(thread_pool),
                        map,
                        sprites,
                        player,
                        graphics: graphics.await?,
                    }),
                )
                .unwrap(),
            ),
            None,
        )
    } else {
        (
            None,
            Some(Context {
                thread_pool: None,
                map,
                sprites,
                player,
                graphics: graphics.await?,
            }),
        )
    };

    unsafe {
        SFB = Some(SharedFrameBuffer {
            frame_buffer: vec![Rgba::default(); ctx.get_dimensions().surface()],
            state: Atomic::new(State::NewFrameAvailable),
        });
    }
    let mut intermediate_fb = vec![Rgba::default(); ctx.get_dimensions().surface()];
    let mut multithreaded = true; // Dummy usage for tests only on mutithread env

    *last_time_clone.borrow_mut() = ctx.loop_hook(
        move |js_array: &mut Uint8ClampedArray, utime: f64| -> bool {
            // Fast copy the last Frame if available
            unsafe {
                if SFB
                    .as_mut()
                    .unwrap()
                    .state
                    .compare_exchange(State::NewFrameAvailable, State::Rendering, SeqCst, SeqCst)
                    .is_ok()
                {
                    let frame_buffer = &mut SFB.as_mut().unwrap().frame_buffer;
                    std::ptr::copy(
                        frame_buffer.as_ptr(),
                        intermediate_fb.as_mut_ptr(),
                        frame_buffer.len(),
                    );
                } else {
                    return false;
                };
            }

            // Get the time between two last frames & evaluate FPS
            let frame_diff_time = utime - *last_time.borrow();
            *last_time.borrow_mut() = utime;
            match start_time {
                Some(_start_time) => {
                    frames_count += 1;
                    let elapsed_time = utime - _start_time;
                    if elapsed_time > 1000. {
                        fps = frames_count as f64 / (elapsed_time / 1000.);
                        frames_count = 0;
                        start_time = Some(utime);
                    }
                }
                None => {
                    start_time = Some(utime);
                }
            }

            // Get IO Kerboard
            let mut commands = Commands::default();

            let mut key_register = key_register.borrow_mut();
            if key_register.keyboard[KeyCode::I as usize].must_be_executed() {
                commands.toggle_linear_interpolation = true;
                key_register.keyboard[KeyCode::I as usize].mark_as_executed();
            }
            if key_register.keyboard[KeyCode::L as usize].must_be_executed() {
                commands.toggle_light = true;
                key_register.keyboard[KeyCode::L as usize].mark_as_executed();
            }
            if key_register.keyboard[KeyCode::M as usize].must_be_executed() {
                commands.toggle_minimap = true;
                key_register.keyboard[KeyCode::M as usize].mark_as_executed();
            }
            if key_register.keyboard[KeyCode::N as usize].must_be_executed() {
                commands.toggle_nostalgia = true;
                key_register.keyboard[KeyCode::N as usize].mark_as_executed();
            }
            if key_register.keyboard[KeyCode::T as usize].must_be_executed() {
                multithreaded = !multithreaded;
                key_register.keyboard[KeyCode::T as usize].mark_as_executed();
            }

            if key_register.keyboard[KeyCode::Left as usize].state == Key::Pressed
                || key_register.keyboard[KeyCode::A as usize].state == Key::Pressed
            {
                commands.left = true;
            }
            if key_register.keyboard[KeyCode::Right as usize].state == Key::Pressed
                || key_register.keyboard[KeyCode::D as usize].state == Key::Pressed
            {
                commands.right = true;
            }
            if key_register.keyboard[KeyCode::Up as usize].state == Key::Pressed
                || key_register.keyboard[KeyCode::W as usize].state == Key::Pressed
            {
                commands.up = true;
            }
            if key_register.keyboard[KeyCode::Down as usize].state == Key::Pressed
                || key_register.keyboard[KeyCode::S as usize].state == Key::Pressed
            {
                commands.down = true;
            }

            // MOBILE DEVICE CONTROL
            if let Some(orientation) = key_register.orientation {
                use web_sys::OrientationType::*;
                // portrait-primary:
                //     The orientation is in the primary portrait mode.
                //     For a smartphone this values means that it’s in a vertical position with the buttons at the bottom.
                // portrait-secondary:
                //     The orientation is in the secondary portrait mode.
                //     For a smartphone this value means that it’s in a vertical position with the buttons at the top
                //     (the device is down under)
                // landscape-primary:
                //     The orientation is in the primary landscape mode.
                //     For a smartphone this value means that it’s in a horizontal position with the buttons at the right.
                // landscape-secondary:
                //     The orientation is in the secondary landscape mode.
                //     For a smartphone this value means that it’s in a horizontal position with the buttons at the left.
                const _EARTH_GRAVITY_ACCELERATION: f64 = 9.806;
                const MIN_INCLINATION_FRONT: f64 = 1.5;
                const MIN_INCLINATION_SIDE: f64 = 2.5;
                if let Some(v) = key_register.acceleration_including_gravity.z {
                    if v < -MIN_INCLINATION_FRONT {
                        commands.down = true;
                    } else if v > MIN_INCLINATION_FRONT {
                        commands.up = true;
                    }
                }
                match orientation {
                    PortraitPrimary => {
                        if let Some(v) = key_register.acceleration_including_gravity.x {
                            if v > MIN_INCLINATION_SIDE {
                                commands.left = true;
                            } else if v < -MIN_INCLINATION_SIDE {
                                commands.right = true;
                            }
                        }
                    }
                    PortraitSecondary => {
                        if let Some(v) = key_register.acceleration_including_gravity.x {
                            if v > MIN_INCLINATION_SIDE {
                                commands.right = true;
                            } else if v < -MIN_INCLINATION_SIDE {
                                commands.left = true;
                            }
                        }
                    }
                    LandscapePrimary => {
                        if let Some(v) = key_register.acceleration_including_gravity.y {
                            if v > MIN_INCLINATION_SIDE {
                                commands.right = true;
                            } else if v < -MIN_INCLINATION_SIDE {
                                commands.left = true;
                            }
                        }
                    }
                    LandscapeSecondary => {
                        if let Some(v) = key_register.acceleration_including_gravity.y {
                            if v > MIN_INCLINATION_SIDE {
                                commands.left = true;
                            } else if v < -MIN_INCLINATION_SIDE {
                                commands.right = true;
                            }
                        }
                    }
                    _ => {}
                }
            }

            let info: Register<_> = *key_register;

            let closure = move |context: &mut Context| {
                // Move sprites
                game::move_sprites::SpritesMove::new(&context.map)
                    .animate_sprites(&mut context.sprites, utime);

                // Move player inside Wolf3d world
                game::move_player::move_player(
                    &context.map,
                    &mut context.player,
                    &commands,
                    frame_diff_time,
                );

                if commands.toggle_linear_interpolation {
                    context.graphics.toggle_linear_interpolation(utime);
                }
                if commands.toggle_light {
                    context.graphics.toggle_light(utime);
                }
                if commands.toggle_minimap {
                    context.graphics.toggle_minimap(utime);
                }
                if commands.toggle_nostalgia {
                    context
                        .graphics
                        .toggle_nostalgia(context.player.height, utime);
                }

                context.graphics.draw_scene(
                    unsafe { &mut SFB.as_mut().unwrap().frame_buffer },
                    match multithreaded {
                        true => context.thread_pool.as_mut(),
                        false => None,
                    },
                    context.player,
                    &context.map,
                    &mut context.sprites,
                    utime,
                    fps,
                    info,
                );

                unsafe {
                    SFB.as_mut()
                        .unwrap()
                        .state
                        .store(State::NewFrameAvailable, SeqCst);
                }
            };
            // On multithread mode, the entire render happens on our worker
            // threads so we don't lock up the main thread.
            match primary_worker_pool.as_ref() {
                Some(worker_pool) => worker_pool.run(closure).unwrap(),
                None => closure(sc_context.as_mut().unwrap()),
            }

            // Draw frame
            let u8_slice = {
                slice_from_raw_parts(
                    intermediate_fb.as_ptr() as *const u8,
                    intermediate_fb.len() * 4,
                )
            };
            js_array.copy_from(unsafe { u8_slice.as_ref().unwrap() });
            true
        },
    )?;
    Ok(())
}

/// Entry point invoked by `worker.js`, a bit of a hack
#[wasm_bindgen]
pub fn child_entry_point(ptr: u32, context_ptr: *mut crate::Context) -> Result<(), JsValue> {
    let ptr = unsafe { Box::from_raw(ptr as *mut pool::Work<crate::Context>) };
    let global = js_sys::global().unchecked_into::<DedicatedWorkerGlobalScope>();
    (ptr.func)(unsafe { context_ptr.as_mut().unwrap() });
    global.post_message(&JsValue::undefined())?;
    Ok(())
}
