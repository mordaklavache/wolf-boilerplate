mod overlay;
mod render;

use overlay::Overlay;
use rayon::ThreadPool;
use render::Render;

use crate::{Elem, KeyStatus, Player, Sprite};

use wasm_bindgen::JsValue;
use wasm_boilerplate::{Coord, GraphicContext, Rgba};

use std::ffi::CString;
use std::ops::Index;

struct Nostalgia {
    factor: NostalgiaFactor,
    scene: Vec<Rgba>,
}

impl Nostalgia {
    pub fn new(dim: Coord<usize>, factor: NostalgiaFactor) -> Self {
        Nostalgia {
            factor,
            scene: vec![Rgba::default(); dim.surface()],
        }
    }
}
pub struct NostalgiaFactor(pub usize);

pub struct Graphics {
    dim: Coord<usize>,
    render: Render,
    nostalgia: Option<Nostalgia>,
    show_minimap: bool,
    string_to_display: Option<(&'static str, f64)>,
}

impl Graphics {
    const DEFAULT_NOSTALGIA_FACTOR: NostalgiaFactor = NostalgiaFactor(4);

    pub async fn new(
        mut dim: Coord<usize>,
        player: Player,
        nostalgia_factor: Option<NostalgiaFactor>,
        graphic_context: &mut GraphicContext,
    ) -> Result<Self, JsValue> {
        let nostalgia = nostalgia_factor.map(|factor| {
            if factor.0 != 2 && factor.0 != 4 && factor.0 != 8 && factor.0 != 16 {
                panic!("Only 2, 4, 8 and 16 are allowed for Nostagia Factor");
            }
            dim.x /= factor.0;
            dim.y /= factor.0;
            Nostalgia::new(dim, factor)
        });

        let render = render::Render::new(
            dim,
            player,
            render::BilinearInterpolation(true),
            graphic_context,
        );

        Ok(Self {
            dim,
            render: render.await?,
            nostalgia,
            show_minimap: true,
            string_to_display: None,
        })
    }
    pub fn draw_scene<T, U>(
        &mut self,
        window: &mut [Rgba],
        thread_pool: Option<&mut ThreadPool>,
        player: Player,
        map: &T,
        sprites: &mut [Sprite],
        utime: f64,
        fps: f64,
        info: wasm_boilerplate::Register<KeyStatus>,
    ) where
        T: Index<usize, Output = U> + std::marker::Sync,
        U: Index<usize, Output = Elem>,
    {
        let scene = match &mut self.nostalgia {
            Some(nostalgia) => unsafe {
                std::slice::from_raw_parts_mut(nostalgia.scene.as_mut_ptr(), self.dim.surface())
            },
            None => window,
        };

        self.render
            .render_scene(scene, thread_pool, player, map, sprites);

        if self.show_minimap {
            Overlay::new(scene, &self.dim).draw_minimap(utime, player, sprites);
        }

        Overlay::new(scene, &self.dim).draw_box(
            Coord::new(self.dim.x as i32 / 2 - 10, self.dim.y as i32 / 2 - 10),
            Coord::new(self.dim.x as i32 / 2 + 10, self.dim.y as i32 / 2 + 10),
            Overlay::RED,
        );

        unsafe {
            let color = Rgba {
                red: 255,
                green: 0,
                blue: 255,
                alpha: 255,
            };
            wasm_boilerplate::vbe_font::display_string(
                scene,
                CString::new(format!("FPS : {}", fps.round())).unwrap(),
                Coord { x: 5, y: 0 },
                self.dim,
                color,
            );
            // wasm_boilerplate::vbe_font::display_string(
            //     scene,
            //     CString::new(format!(
            //         "Acceleration with Gravity - x {:?} y {:?} z {:?}",
            //         info.acceleration_including_gravity.x,
            //         info.acceleration_including_gravity.y,
            //         info.acceleration_including_gravity.z
            //     ))
            //     .unwrap(),
            //     Coord { x: 5, y: 16 },
            //     self.dim,
            //     color,
            // );

            // wasm_boilerplate::vbe_font::display_string(
            //     scene,
            //     CString::new(format!(
            //         "Acceleration - x {:?} y {:?} z {:?}",
            //         info.acceleration.x, info.acceleration.y, info.acceleration.z
            //     ))
            //     .unwrap(),
            //     Coord { x: 5, y: 32 },
            //     self.dim,
            //     color,
            // );

            // wasm_boilerplate::vbe_font::display_string(
            //     scene,
            //     CString::new(format!(
            //         "Acceleration Sum - x {:?} y {:?} z {:?}",
            //         info.acceleration_sum.x, info.acceleration_sum.y, info.acceleration_sum.z
            //     ))
            //     .unwrap(),
            //     Coord { x: 5, y: 48 },
            //     self.dim,
            //     color,
            // );

            // wasm_boilerplate::vbe_font::display_string(
            //     scene,
            //     CString::new(format!("Orientation - {:?}", info.orientation)).unwrap(),
            //     Coord { x: 5, y: 64 },
            //     self.dim,
            //     color,
            // );

            if let Some((string, time)) = &self.string_to_display {
                let time = utime - time;
                if time < 1500. {
                    wasm_boilerplate::vbe_font::display_string(
                        scene,
                        CString::new(string.as_bytes()).unwrap(),
                        Coord::new(10, 10 + wasm_boilerplate::vbe_font::CHAR_HEIGHT),
                        self.dim,
                        color,
                    );
                }
            } else {
                self.string_to_display = None;
            }
        }
        if let Some(nostalgia) = &self.nostalgia {
            let shift = nostalgia.factor.0.trailing_zeros() as usize;
            for y in 0..self.dim.y << shift {
                for x in 0..self.dim.x << shift {
                    window[y * (self.dim.x << shift) + x] =
                        nostalgia.scene[(y >> shift) * self.dim.x + (x >> shift)];
                }
            }
        }
    }

    pub fn toggle_minimap(&mut self, utime: f64) {
        self.show_minimap = !self.show_minimap;
        self.string_to_display = Some((
            match self.show_minimap {
                true => "Minimap: ON",
                false => "Minimap: OFF",
            },
            utime,
        ));
    }

    pub fn toggle_linear_interpolation(&mut self, utime: f64) {
        self.render.bilinear_interpolation.0 = !self.render.bilinear_interpolation.0;
        self.string_to_display = Some((
            match self.render.bilinear_interpolation.0 {
                true => "Bilinear interpolation: ON",
                false => "Bilinear interpolation: OFF",
            },
            utime,
        ));
    }

    pub fn toggle_light(&mut self, utime: f64) {
        if self.render.shadow_limit == Render::HIGH_SHADOW_LIMIT {
            self.render.shadow_limit = Render::LOW_SHADOW_LIMIT;
            self.string_to_display = Some(("Light: OFF", utime));
        } else if self.render.shadow_limit == Render::LOW_SHADOW_LIMIT {
            self.render.shadow_limit = Render::HIGH_SHADOW_LIMIT;
            self.string_to_display = Some(("Light: ON", utime));
        }
    }

    pub fn toggle_nostalgia(&mut self, player_height: f32, utime: f64) {
        match &self.nostalgia {
            None => {
                let new_dim = Coord {
                    x: self.dim.x / Self::DEFAULT_NOSTALGIA_FACTOR.0,
                    y: self.dim.y / Self::DEFAULT_NOSTALGIA_FACTOR.0,
                };
                self.dim = new_dim;
                self.render.new_dimensions(new_dim, player_height);
                self.nostalgia = Some(Nostalgia::new(new_dim, Self::DEFAULT_NOSTALGIA_FACTOR));
                self.string_to_display = Some(("Enable Nostalgia mode", utime));
            }
            Some(Nostalgia {
                factor: mostalgia_factor,
                scene: _,
            }) => {
                let new_dim = Coord {
                    x: self.dim.x * mostalgia_factor.0,
                    y: self.dim.y * mostalgia_factor.0,
                };
                self.dim = new_dim;
                self.render.new_dimensions(new_dim, player_height);
                self.nostalgia = None;
                self.string_to_display = Some(("Disable Nostalgia mode", utime));
            }
        }
    }
}
