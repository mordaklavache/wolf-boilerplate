use wasm_boilerplate::*;

use super::WallCollision;
use crate::{Commands, Elem, Player};

use std::ops::Index;

const ROTATION_ANGLE_BY_MS: f32 = 2.0 * std::f32::consts::PI / 1000.;
const DISTANCE_BY_MS: f32 = 0.015;

pub fn move_player<T, U>(map: &T, player: &mut Player, commands: &Commands, time_elapsed: f64)
where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    if commands.left {
        let new_q = time_elapsed as f32 * ROTATION_ANGLE_BY_MS;
        let new_l = 0.;
        set_player_data(map, player, new_q, new_l);
    }
    if commands.right {
        let new_q = time_elapsed as f32 * -ROTATION_ANGLE_BY_MS;
        let new_l = 0.;
        set_player_data(map, player, new_q, new_l);
    }
    if commands.up {
        let new_q = 0.;
        let new_l = time_elapsed as f32 * DISTANCE_BY_MS;
        set_player_data(map, player, new_q, new_l);
    }
    if commands.down {
        let new_q = 0.;
        let new_l = time_elapsed as f32 * -DISTANCE_BY_MS;
        set_player_data(map, player, new_q, new_l);
    }
}

fn set_player_data<T, U>(map: &T, player: &mut Player, q: f32, l: f32)
where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    let mut new = Coord::default();

    player.angle -= q;

    if player.angle < 0.0 {
        player.angle += 2.0 * std::f32::consts::PI;
    } else if player.angle >= 2.0 * std::f32::consts::PI {
        player.angle -= 2.0 * std::f32::consts::PI;
    }

    new.x = player.angle.cos() * l;
    new.y = player.angle.sin() * l;

    let w = WallCollision::new(map).get_wall_info(*player);
    new = WallCollision::new(map).test_mvt(player.location, w, new);

    let w = WallCollision::new(map).get_wall_info(Player {
        location: player.location,
        angle: player.angle + std::f32::consts::PI,
        ..Default::default()
    });
    new = WallCollision::new(map).test_mvt(player.location, w, new);

    player.location.x += new.x;
    player.location.y += new.y;
}
