use crate::{Elem, Player};

use wasm_boilerplate::*;

use std::ops::Index;

#[derive(Debug, Copy, Clone, Default)]
struct Vector2 {
    _module: f32, // TODO: Understand why is not used
    dx: f32,
    dy: f32,
}

impl Vector2 {
    fn new(angle: f32, dist: f32) -> Self {
        Self {
            dx: angle.cos() * dist,
            dy: angle.sin() * dist,
            _module: dist,
        }
    }
}

#[derive(Debug, Copy, Clone, Default)]
pub struct WallVector {
    norm: Coord<f32>,
    v: Vector2,
}

#[derive(Debug, Copy, Clone, Default)]
struct WallInfo {
    ray_pos: Coord<f32>,
    ray_dir: Coord<f32>,
    map: Coord<i32>,
    step: Coord<i32>,
    delta_dist: Coord<f32>,
    side_dist: Coord<f32>,
    w: WallVector,
}

pub struct WallCollision<'a, T, U>
where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    map: &'a T,
}

impl<'a, T, U> WallCollision<'a, T, U>
where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    pub fn new(map: &'a T) -> Self {
        Self { map }
    }
    pub fn get_wall_info(&self, player: Player) -> WallVector {
        let mut i = WallInfo {
            ray_pos: player.location,
            ray_dir: Coord::new(player.angle.cos(), player.angle.sin()),
            map: Coord::new(player.location.x as i32, player.location.y as i32),
            ..Default::default()
        };

        if i.ray_dir.y == 0.0 {
            i.step.x = if i.ray_dir.x > 0.0 { 1 } else { -1 };

            while let Elem::Floor(_) = self.map[i.map.y as usize][i.map.x as usize] {
                i.map.x += i.step.x;
            }

            let norm = Coord {
                x: if i.ray_dir.x > 0.0 { -1.0 } else { 1.0 },
                y: 0.0,
            };
            let v = Vector2::new(player.angle, (i.map.x as f32 - i.ray_pos.x).abs());
            WallVector { norm, v }
        } else if i.ray_dir.x == 0.0 {
            i.step.y = if i.ray_dir.y > 0.0 { 1 } else { -1 };

            while let Elem::Floor(_) = self.map[i.map.y as usize][i.map.x as usize] {
                i.map.y += i.step.y;
            }

            let norm = Coord {
                x: 0.0,
                y: if i.ray_dir.y > 0.0 { -1.0 } else { 1.0 },
            };
            let v = Vector2::new(player.angle, (i.map.y as f32 - i.ray_pos.y).abs());

            WallVector { norm, v }
        } else {
            self.middle_seq(player.angle, i)
        }
    }

    pub fn test_mvt(
        &self,
        obj_location: Coord<f32>,
        w: WallVector,
        mut new: Coord<f32>,
    ) -> Coord<f32> {
        if w.norm.y == 1.0 && new.y < (w.v.dy + 0.4) {
            new.y = w.v.dy + 0.4;
            new.x = if new.x > 0.0 {
                self.mvt_right(new, obj_location)
            } else {
                self.mvt_left(new, obj_location)
            };
        }

        if w.norm.x == 1.0 && new.x < (w.v.dx + 0.4) {
            new.x = w.v.dx + 0.4;
            new.y = if w.v.dy > 0.0 {
                self.mvt_back(new, obj_location)
            } else {
                self.mvt_top(new, obj_location)
            };
        }

        if w.norm.y == -1.0 && new.y > (w.v.dy - 0.4) {
            new.y = w.v.dy - 0.4;
            new.x = if new.x > 0.0 {
                self.mvt_right(new, obj_location)
            } else {
                self.mvt_left(new, obj_location)
            };
        }

        if w.norm.x == -1.0 && new.x > (w.v.dx - 0.4) {
            new.x = w.v.dx - 0.4;
            new.y = if w.v.dy > 0.0 {
                self.mvt_back(new, obj_location)
            } else {
                self.mvt_top(new, obj_location)
            };
        }
        new
    }

    fn end_seq(&self, angle: f32, mut i: WallInfo) -> WallVector {
        let mut side = 0;

        while let Elem::Floor(_) = self.map[i.map.y as usize][i.map.x as usize] {
            if i.side_dist.x < i.side_dist.y {
                i.side_dist.x += i.delta_dist.x;
                i.map.x += i.step.x;
                side = 0;
            } else {
                i.side_dist.y += i.delta_dist.y;
                i.map.y += i.step.y;
                side = 1;
            }
        }

        i.w.norm = if side == 1 {
            Coord::new(0.0, -1. * i.step.y as f32)
        } else {
            Coord::new(-1. * i.step.x as f32, 0.0)
        };

        let v = if side == 1 {
            Vector2::new(
                angle,
                (i.map.y as f32 - i.ray_pos.y + (1.0 - i.step.y as f32) / 2.0) / i.ray_dir.y,
            )
        } else {
            Vector2::new(
                angle,
                (i.map.x as f32 - i.ray_pos.x + (1.0 - i.step.x as f32) / 2.0) / i.ray_dir.x,
            )
        };
        WallVector { norm: i.w.norm, v }
    }

    fn middle_seq(&self, angle: f32, mut i: WallInfo) -> WallVector {
        i.delta_dist.x = (1.0 + (i.ray_dir.y * i.ray_dir.y) / (i.ray_dir.x * i.ray_dir.x)).sqrt();
        i.delta_dist.y = (1.0 + (i.ray_dir.x * i.ray_dir.x) / (i.ray_dir.y * i.ray_dir.y)).sqrt();

        if i.ray_dir.x < 0.0 {
            i.step.x = -1;
            i.side_dist.x = (i.ray_pos.x - i.map.x as f32) * i.delta_dist.x;
        } else {
            i.step.x = 1;
            i.side_dist.x = (i.map.x as f32 + 1.0 - i.ray_pos.x) * i.delta_dist.x;
        }

        if i.ray_dir.y < 0.0 {
            i.step.y = -1;
            i.side_dist.y = (i.ray_pos.y - i.map.y as f32) * i.delta_dist.y;
        } else {
            i.step.y = 1;
            i.side_dist.y = (i.map.y as f32 + 1.0 - i.ray_pos.y) * i.delta_dist.y;
        }
        self.end_seq(angle, i)
    }

    fn mvt_right(&self, mvt: Coord<f32>, location: Coord<f32>) -> f32 {
        let w = self.get_wall_info(Player {
            angle: 0.0,
            location: Coord {
                x: location.x,
                y: location.y + mvt.y,
            },
            ..Default::default()
        });

        if (w.v.dx - 0.4) < mvt.x {
            return w.v.dx - 0.4;
        } else {
            return mvt.x;
        }
    }

    fn mvt_left(&self, mvt: Coord<f32>, location: Coord<f32>) -> f32 {
        let w = self.get_wall_info(Player {
            angle: std::f32::consts::PI,
            location: Coord {
                x: location.x,
                y: location.y + mvt.y,
            },
            ..Default::default()
        });

        if mvt.x < (w.v.dx + 0.4) {
            return w.v.dx + 0.4;
        } else {
            return mvt.x;
        }
    }

    fn mvt_top(&self, mvt: Coord<f32>, location: Coord<f32>) -> f32 {
        let w = self.get_wall_info(Player {
            angle: std::f32::consts::PI * 1.5,
            location: Coord {
                x: location.x + mvt.x,
                y: location.y,
            },
            ..Default::default()
        });

        if (w.v.dy + 0.4) > mvt.y {
            return w.v.dy + 0.4;
        } else {
            return mvt.y;
        }
    }

    fn mvt_back(&self, mvt: Coord<f32>, location: Coord<f32>) -> f32 {
        let w = self.get_wall_info(Player {
            angle: std::f32::consts::PI * 0.5,
            location: Coord {
                x: location.x + mvt.x,
                y: location.y,
            },
            ..Default::default()
        });

        if mvt.y > (w.v.dy - 0.4) {
            return w.v.dy - 0.4;
        } else {
            return mvt.y;
        }
    }
}
