use crate::{Elem, Sprite};

use wasm_boilerplate::Coord;

use std::ops::Index;

pub struct SpritesMove<'a, T, U>
where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    map: &'a T,
}

impl<'a, T, U> SpritesMove<'a, T, U>
where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    const SPRITE_TILE_BY_SEC: f64 = 3.;

    pub fn new(map: &'a T) -> Self {
        Self { map }
    }

    pub fn animate_sprites(&mut self, sprites: &mut [Sprite], mtime: f64) {
        let ms = 1000. / Self::SPRITE_TILE_BY_SEC;
        for sprite in sprites {
            match sprite.last_time {
                Some(last_time) => {
                    if mtime - last_time > ms {
                        sprite.map_location.x = sprite.target_location.x as usize;
                        sprite.map_location.y = sprite.target_location.y as usize;

                        // TODO: Quick fix for the moment:
                        // For now, it's as if the sprites were pausing when we lose focus on the browser.
                        // self.make_decision(sprite, mtime + (mtime - last_time) - ms);
                        self.make_decision(sprite, mtime);
                    } else {
                        sprite.current_location.x = sprite.origin_location.x
                            + (mtime - last_time) as f32
                                * (sprite.target_location.x - sprite.origin_location.x)
                                / ms as f32;

                        sprite.current_location.y = sprite.origin_location.y
                            + (mtime - last_time) as f32
                                * (sprite.target_location.y - sprite.origin_location.y)
                                / ms as f32;
                    }
                }
                None => {
                    self.make_decision(sprite, mtime);
                }
            }
        }
    }

    fn make_decision(&self, sprite: &mut Sprite, mtime: f64) {
        sprite.origin_location = Coord {
            x: sprite.map_location.x as f32 + 0.5,
            y: sprite.map_location.y as f32 + 0.5,
        };
        sprite.current_location = sprite.origin_location;

        let mut res = [sprite.map_location; 4];
        let mut i = 0;
        // move down
        if let Elem::Floor(_) = self.map[sprite.map_location.y + 1][sprite.map_location.x] {
            res[i].y += 1;
            i += 1;
        }
        // move up
        if let Elem::Floor(_) = self.map[sprite.map_location.y - 1][sprite.map_location.x] {
            res[i].y -= 1;
            i += 1;
        }
        // move right
        if let Elem::Floor(_) = self.map[sprite.map_location.y][sprite.map_location.x + 1] {
            res[i].x += 1;
            i += 1;
        }
        // move left
        if let Elem::Floor(_) = self.map[sprite.map_location.y][sprite.map_location.x - 1] {
            res[i].x -= 1;
            i += 1;
        }

        sprite.last_time = Some(mtime);
        let target_location = res[rand::random::<usize>() % i];
        sprite.target_location = Coord {
            x: target_location.x as f32 + 0.5,
            y: target_location.y as f32 + 0.5,
        };
    }
}
