mod find_wall;
mod render_floor;
mod render_pix;
mod render_sky;
mod render_sprites;
mod render_walls;

use crate::{Elem, FloorKind, Player, Sprite, SpriteKind, Wall, WallKind};

use futures_util::future::{join3, join5};
// use js_sys::Date;
use rayon::{
    prelude::{IndexedParallelIterator, ParallelIterator},
    slice::ParallelSliceMut,
    ThreadPool,
};
use wasm_bindgen::prelude::*;
use wasm_boilerplate::{fetch_resource, Coord, GraphicContext, Rgba};
// use wasm_boilerplate::console_log;

use std::cell::RefCell;
use std::collections::BTreeMap;
use std::ffi::OsStr;
use std::ops::Index;
use std::rc::Rc;

pub struct Render {
    dim: Coord<usize>,

    // pre-calculus
    calc: Calc,

    // Textures
    floors: Vec<(Vec<Rgba>, Coord<usize>)>,
    walls: Vec<(Vec<Rgba>, Coord<usize>)>,
    sprites: Vec<(Vec<Rgba>, Coord<usize>)>,
    skybox: SkyBox,

    // Parameters
    pub bilinear_interpolation: BilinearInterpolation,
    pub shadow_limit: f32,
}

struct Calc {
    angle_x: Vec<f32>,
    angle_y: Vec<f32>,
    dist_floor: Vec<f32>,
    atan_list: Vec<f32>,
    cos_list: Vec<f32>,

    // Temporary values
    columns: Vec<Columns>,
}

struct SkyBox {
    image: Vec<Rgba>,
    original: Vec<u8>,
    original_dim: Coord<usize>,
}

pub struct BilinearInterpolation(pub bool);

pub struct Columns {
    wall_x_offset: f32,
    wall_h_dist: f32,
    wall_x_tex: f32,
    wall_min_angle: f32,
    wall_max_angle: f32,
    wall: Wall,
}

// static mut COUNTER_RENDER: usize = 0;
// static mut COUNTER_SPRITE: usize = 0;
// static mut RESULT_RENDER: f64 = 0.;
// static mut RESULT_SPRITE: f64 = 0.;

impl Render {
    const VIEW_ANGLE: f32 = 3. / 6. * std::f32::consts::PI;
    pub const HIGH_SHADOW_LIMIT: f32 = 7.0;
    pub const LOW_SHADOW_LIMIT: f32 = 2.0;

    pub async fn new(
        dim: Coord<usize>,
        player: Player,
        bilinear_interpolation: BilinearInterpolation,
        graphic_context: &mut GraphicContext,
    ) -> Result<Self, JsValue> {
        let graphic_context = Rc::new(RefCell::new(graphic_context));

        graphic_context
            .borrow_mut()
            .raw_string("LOADING. PLEASE WAIT...");
        graphic_context.borrow_mut().raw_string("");
        let calc = Calc::new(dim, player.height);

        let skybox_url = match cfg!(debug_assertions) {
            true => "ressources/skybox/astro_reduced.png",
            false => "ressources/skybox/astro_reduced.png",
        };
        graphic_context.borrow_mut().raw_string("Fetching Skybox");
        let skybox = wasm_boilerplate::fetch_resource(
            skybox_url,
            |v: Vec<u8>, _filename: &OsStr| -> SkyBox {
                let (original, skybox_info) = wasm_boilerplate::png_reader::get_image_buffer(&v);
                let src_dim = Coord {
                    x: skybox_info.width as usize,
                    y: skybox_info.height as usize,
                };
                graphic_context
                    .borrow_mut()
                    .raw_string("- Sky creation done");
                SkyBox {
                    image: render_sky::create_skybox(&original, src_dim, dim),
                    original,
                    original_dim: src_dim,
                }
            },
        )
        .await?;

        let closure = |v: Vec<u8>, filename: &OsStr| -> (Vec<Rgba>, Coord<usize>) {
            graphic_context
                .borrow_mut()
                .raw_string(format!("- {} loaded", filename.to_string_lossy()));
            let (bitmap, info) = wasm_boilerplate::png_reader::get_image_buffer(&v);
            let dim = Coord::new(info.width as usize, info.height as usize);
            let mut rgba: Vec<Rgba> = Vec::with_capacity(dim.surface());
            unsafe {
                rgba.set_len(dim.surface());
            }
            transpose_image(&bitmap, &mut rgba, dim, dim, None);
            (rgba, dim)
        };

        graphic_context.borrow_mut().raw_string("Fetching Sprites");
        let s1 = fetch_resource("ressources/sprites/pig_sprite.png", closure);
        let s2 = fetch_resource("ressources/sprites/dog_sprite.png", closure);
        let s3 = fetch_resource("ressources/sprites/sadirac_sprite.png", closure);
        let (s1, s2, s3) = join3(s1, s2, s3).await;
        let sprites = [
            (SpriteKind::Pig, s1?),
            (SpriteKind::Dog, s2?),
            (SpriteKind::Sadirak, s3?),
        ]
        .into_iter()
        .collect::<BTreeMap<SpriteKind, (Vec<Rgba>, Coord<usize>)>>();

        graphic_context
            .borrow_mut()
            .raw_string("Fetching Floors Textures");
        let f1 = fetch_resource("ressources/floors/parquet.png", closure);
        let f2 = fetch_resource("ressources/floors/seamless_carpet.png", closure);
        let f3 = fetch_resource("ressources/floors/bricks.png", closure);
        let (f1, f2, f3) = join3(f1, f2, f3).await;
        let floors = [
            (FloorKind::Parquet, f1?),
            (FloorKind::Carpet, f2?),
            (FloorKind::Stone, f3?),
        ]
        .into_iter()
        .collect::<BTreeMap<FloorKind, (Vec<Rgba>, Coord<usize>)>>();

        graphic_context
            .borrow_mut()
            .raw_string("Fetching Walls Textures");
        let w1 = fetch_resource("ressources/walls/4murs.png", closure);
        let w2 = fetch_resource("ressources/walls/4murs2.png", closure);
        let w3 = fetch_resource("ressources/walls/pig.png", closure);
        let w4 = fetch_resource("ressources/walls/aperture.png", closure);
        let w5 = fetch_resource("ressources/walls/panic.png", closure);

        let (w1, w2, w3, w4, w5) = join5(w1, w2, w3, w4, w5).await;
        let walls: BTreeMap<WallKind, (Vec<Rgba>, Coord<usize>)> = [
            (WallKind::FourWall, w1?),
            (WallKind::FourWall2, w2?),
            (WallKind::Piggie, w3?),
            (WallKind::Aperture, w4?),
            (WallKind::Panic, w5?),
        ]
        .into_iter()
        .collect::<BTreeMap<WallKind, (Vec<Rgba>, Coord<usize>)>>();

        Ok(Self {
            dim,
            calc,
            walls: walls.into_values().collect(), // Use Vec instead of HashMap for optim
            floors: floors.into_values().collect(), // Use Vec instead of HashMap for optim
            sprites: sprites.into_values().collect(), // Use Vec instead of HashMap for optim
            skybox,
            bilinear_interpolation,
            shadow_limit: Self::HIGH_SHADOW_LIMIT,
        })
    }

    pub fn render_scene<T, U>(
        &mut self,
        scene: &mut [Rgba],
        thread_pool: Option<&mut ThreadPool>,
        player: Player,
        map: &T,
        sprites: &mut [Sprite],
    ) where
        T: Index<usize, Output = U> + std::marker::Sync,
        U: Index<usize, Output = Elem>,
    {
        let sky_pos = (self.dim.x as f32 / Self::VIEW_ANGLE * player.angle).round() as i32;
        let ratio = (2. * std::f32::consts::PI / Self::VIEW_ANGLE) as usize;

        self.calc_columns(&player, map);

        // let t1 = Date::now();

        match thread_pool {
            Some(thread_pool) => thread_pool.install(|| {
                scene
                    .par_chunks_mut(self.dim.x)
                    .enumerate()
                    .for_each(|(y, chunk)| {
                        let angle_y = self.calc.angle_y[y];
                        for x in 0..chunk.len() {
                            let angle_x = self.calc.angle_x[x] + player.angle;
                            if angle_y <= self.calc.columns[x].wall_min_angle {
                                self.mt_render_floor(
                                    chunk,
                                    player,
                                    map,
                                    Coord::new(x as i32, y as i32),
                                    Coord::new(angle_x, angle_y),
                                );
                            } else if angle_y <= self.calc.columns[x].wall_max_angle {
                                self.mt_render_wall(chunk, player, Coord::new(x as i32, y as i32));
                            } else {
                                let idx = sky_pos as usize + y * self.dim.x * (ratio + 1) + x;
                                chunk[x] = self.skybox.image[idx];
                            }
                        }
                    });
            }),
            None => {
                // NO MULTITHREADED CODE FOR BACKWARD COMPATIBILITY
                for y in 0..self.dim.y {
                    let angle_y = self.calc.angle_y[y];

                    for x in 0..self.dim.x {
                        let angle_x = self.calc.angle_x[x] + player.angle;

                        if angle_y <= self.calc.columns[x].wall_min_angle {
                            self.render_floor(
                                scene,
                                player,
                                map,
                                Coord::new(x as i32, y as i32),
                                Coord::new(angle_x, angle_y),
                            );
                        } else if angle_y <= self.calc.columns[x].wall_max_angle {
                            self.render_wall(scene, player, Coord::new(x as i32, y as i32));
                        } else {
                            let idx = sky_pos as usize + y * self.dim.x * (ratio + 1) + x;
                            scene[y * self.dim.x + x] = self.skybox.image[idx];
                        }
                    }
                }
            }
        }

        // unsafe {
        //     COUNTER_RENDER += 1;
        //     RESULT_RENDER += Date::now() - t1;

        //     if COUNTER_RENDER == 30 {
        //         console_log!("RENDER Ponderate time: {} ms", RESULT_RENDER / COUNTER_RENDER as f64);
        //         COUNTER_RENDER = 0;
        //         RESULT_RENDER = 0.;
        //     }
        // }
        //let t1 = Date::now();
        self.render_sprites(scene, player, sprites);
        // unsafe {
        //     COUNTER_SPRITE += 1;
        //     RESULT_SPRITE += Date::now() - t1;

        //     if COUNTER_SPRITE == 20 {
        //         console_log!("SPRITE Ponderate time: {} ms", RESULT_SPRITE / COUNTER_SPRITE as f64);
        //         COUNTER_SPRITE = 0;
        //         RESULT_SPRITE = 0.;
        //     }
        // }
    }

    pub fn new_dimensions(&mut self, new_dim: Coord<usize>, player_height: f32) {
        self.dim = new_dim;
        self.calc = Calc::new(new_dim, player_height);
        self.skybox.image =
            render_sky::create_skybox(&self.skybox.original, self.skybox.original_dim, new_dim);
    }

    fn calc_columns<T, U>(&mut self, player: &Player, map: &T)
    where
        T: Index<usize, Output = U>,
        U: Index<usize, Output = Elem>,
    {
        for x in 0..self.dim.x {
            let mut c = &mut self.calc.columns[x];

            let angle_x = self.calc.angle_x[x] + player.angle;

            let mut c_intersect = Coord::default();

            find_wall::find_wall(&mut c, map, player, angle_x, &mut c_intersect);

            c.wall_h_dist = dist(player.location, c_intersect) * self.calc.cos_list[x];

            c.wall_min_angle = (-player.height / c.wall_h_dist).atan();
            c.wall_max_angle = ((c.wall.height - player.height) / c.wall_h_dist).atan();
        }
    }
}

impl Calc {
    const VIEW_ANGLE: f32 = Render::VIEW_ANGLE;

    pub fn new(dim: Coord<usize>, player_height: f32) -> Self {
        let angle_x = ((-1 * (dim.x >> 1) as i32)..((dim.x >> 1) as i32)) // -dim.x / 2.0 ..to.. dim.x / 2.0
            .map(|x| Self::angle_on_screen(x as f32, dim.x as f32))
            .collect::<Vec<f32>>();
        let angle_y = (0..dim.y)
            .map(|y| Self::angle_on_screen((dim.y as f32 / 2.) - y as f32, dim.x as f32))
            .collect::<Vec<f32>>();
        let cos_list = (0..dim.x).map(|x| angle_x[x].cos()).collect();
        let dist_floor = (0..dim.y)
            .map(|y| player_height / (-angle_y[y]).tan())
            .collect();
        let atan_list = (0..dim.y).map(|y| angle_y[y].tan()).collect();
        Self {
            angle_x,
            angle_y,
            cos_list,
            dist_floor,
            atan_list,
            columns: {
                let mut columns = Vec::with_capacity(dim.x);
                unsafe {
                    columns.set_len(dim.x);
                }
                columns
            },
        }
    }

    fn angle_on_screen(v: f32, width: f32) -> f32 {
        (v / (width / 2.) * (Self::VIEW_ANGLE / 2.).tan()).atan()
    }
}

fn transpose_image(
    src: &[u8],
    dst: &mut [Rgba],
    src_dim: Coord<usize>,
    dst_dim: Coord<usize>,
    max_width: Option<usize>,
) {
    let out_width = match max_width {
        Some(width) => width,
        None => dst_dim.x,
    };

    let scale_x: f64 = src_dim.x as f64 / dst_dim.x as f64;
    let scale_y: f64 = src_dim.y as f64 / dst_dim.y as f64;

    for y2 in 0..dst_dim.y {
        for x2 in 0..dst_dim.x {
            // let x1 = (x2 as f64 * scale_x).round() as usize; // TODO: round() causes index out of bound with VIEW_ANGLE 60deg
            // let y1 = (y2 as f64 * scale_y).round() as usize; // TODO: round() causes index out of bound with VIEW_ANGLE 60deg
            let x1 = (x2 as f64 * scale_x) as usize;
            let y1 = (y2 as f64 * scale_y) as usize;
            let source_index = (y1 * src_dim.x + x1) * 3; // 3 channels (RGB)
            let destination_index = y2 * out_width + x2;

            // Copy pixel values from source to destination
            dst[destination_index] = Rgba {
                red: src[source_index],
                green: src[source_index + 1],
                blue: src[source_index + 2],
                alpha: 255,
            };
        }
    }
}

fn dist(a: Coord<f32>, b: Coord<f32>) -> f32 {
    let delta = Coord::new(b.x - a.x, b.y - a.y);
    (delta.x.powf(2.0) + delta.y.powf(2.0)).sqrt()
}
