use super::*;
use crate::{Player, Sprite, SpriteKind};

impl<'a> super::Overlay<'a> {
    const BORDER_PERCENT: f32 = 0.15;
    const RADIUS_RELATIVE_SIZE: f32 = 0.85;

    const ENEMY_CIRCLE_RATIO: f32 = 25.;
    pub const ARROW_RATIO: f32 = 0.15;
    pub const LONG_ARROW_RATIO: f32 = 0.30;

    pub fn draw_minimap(&mut self, mtime: f64, player: Player, sprites: &[Sprite]) {
        let mut map_origin_x = self.dim.x as f32 * (1. - Self::BORDER_PERCENT);
        let mut map_origin_y = self.dim.y as f32 * Self::BORDER_PERCENT;
        let min = (self.dim.x as f32 - map_origin_x as f32).min(map_origin_y);
        map_origin_x = self.dim.x as f32 - min;
        map_origin_y = min;
        let map_radius = Self::RADIUS_RELATIVE_SIZE * min;

        let l1 = Coord {
            x: map_origin_x as i32,
            y: map_origin_y as i32,
        };

        self.draw_arrow(l1, player.angle, map_radius);

        let color = Self::WHITE;
        self.draw_circle(l1, map_radius, color);

        let angle = (((mtime as u64 % Self::RADAR_TURN_DURATION_MS as u64) as f64)
            / Self::RADAR_TURN_DURATION_MS
            * std::f64::consts::PI
            * 2.) as f32;

        let line = Line {
            b_pix: Rgba {
                green: 255,
                blue: 255,
                ..Default::default()
            },
            f_pix: Rgba {
                red: 255,
                green: 255,
                ..Default::default()
            },
            p1: l1,
            p2: Coord {
                x: (angle.cos() * map_radius + map_origin_x) as i32,
                y: (angle.sin() * map_radius + map_origin_y) as i32,
            },
            ..Default::default()
        };
        self.draw_line(line);

        // Locate and draw
        let ref_angle = angle.sin().atan2(angle.cos());

        for sprite in sprites {
            let mut l = Coord {
                x: sprite.current_location.x - player.location.x,
                y: sprite.current_location.y - player.location.y,
            };

            let dist = (l.x * l.x + l.y * l.y).sqrt();

            if dist < MAP_DEPTH as f32 - 1.0 {
                let angle = l.y.atan2(l.x);

                let distance_factor = is_close(
                    -std::f32::consts::PI,
                    std::f32::consts::PI,
                    angle,
                    ref_angle,
                );
                if distance_factor != 0. {
                    let (pix, radius_factor) = match sprite.kind {
                        SpriteKind::Sadirak => (
                            Rgba {
                                red: (255. * distance_factor) as u8,
                                ..Default::default()
                            },
                            1.2,
                        ),
                        SpriteKind::Dog => (
                            Rgba {
                                blue: (255. * distance_factor) as u8,
                                ..Default::default()
                            },
                            1.,
                        ),
                        _ => (
                            Rgba {
                                green: (255. * distance_factor) as u8,
                                ..Default::default()
                            },
                            1.,
                        ),
                    };
                    l.x = l.x * (map_radius / MAP_DEPTH as f32) + map_origin_x;
                    l.y = l.y * (map_radius / MAP_DEPTH as f32) + map_origin_y;
                    self.draw_circle(
                        Coord {
                            x: l.x as i32,
                            y: l.y as i32,
                        },
                        map_radius / Self::ENEMY_CIRCLE_RATIO * radius_factor,
                        pix,
                    );
                }
            }
        }
    }
}

fn is_close(min: f32, max: f32, a: f32, b: f32) -> f32 {
    let v = if a > b { a - b } else { b - a };
    let u = (max - min) - v;
    let u = if v < u { v } else { u };

    if u < 1.0 {
        1.0 - (u / 1.0)
    } else {
        0.0
    }
}
