use super::Line;

use wasm_boilerplate::*;

impl<'a> super::Overlay<'a> {
    pub fn draw_line(&mut self, mut p: Line) {
        p.d = Coord {
            x: p.p2.x - p.p1.x,
            y: p.p2.y - p.p1.y,
        };

        let inc = Coord {
            x: if p.d.x < 0 { -1 } else { 1 },
            y: if p.d.y < 0 { -1 } else { 1 },
        };

        p.d.x = p.d.x.abs();
        p.d.y = p.d.y.abs();

        self.plot_pixel(p.p1, p.b_pix);

        if p.d.x != 0 || p.d.y != 0 {
            if p.d.x > p.d.y {
                self.horizontal_line(p, inc);
            } else {
                self.vertical_line(p, inc);
            }
        }
    }

    fn horizontal_line(&mut self, p: Line, inc: Coord<i32>) {
        let mut cumul = p.d.x / 2;
        let mut c = p.p1;

        while c.x != p.p2.x {
            c.x += inc.x;
            cumul += p.d.y;

            if cumul >= p.d.x {
                c.y += inc.y;
                cumul -= p.d.x;
            }

            let pix = get_pix_line(&p, c.x as f32, p.p1.x as f32, p.p2.x as f32);
            self.plot_pixel(c, pix);
        }
    }

    fn vertical_line(&mut self, p: Line, inc: Coord<i32>) {
        let mut cumul = p.d.y / 2;
        let mut c = p.p1;

        while c.y != p.p2.y {
            c.y += inc.y;
            cumul += p.d.x;

            if cumul >= p.d.y {
                c.x += inc.x;
                cumul -= p.d.y;
            }

            let pix = get_pix_line(&p, c.y as f32, p.p1.y as f32, p.p2.y as f32);
            self.plot_pixel(c, pix);
        }
    }
}

#[inline]
fn get_pix_line(p: &Line, x: f32, x_beg: f32, x_end: f32) -> Rgba {
    let b_pix = p.b_pix;
    let f_pix = p.f_pix;

    let ratio = (x - x_beg) / (x_end - x_beg);

    let new_pix = Rgba {
        red: ((1.0 - ratio) * b_pix.red as f32 + ratio * f_pix.red as f32) as _,
        green: ((1.0 - ratio) * b_pix.green as f32 + ratio * f_pix.green as f32) as _,
        blue: ((1.0 - ratio) * b_pix.blue as f32 + ratio * f_pix.blue as f32) as _,
        alpha: ((1.0 - ratio) * b_pix.alpha as f32 + ratio * f_pix.alpha as f32) as _,
    };
    new_pix
}
