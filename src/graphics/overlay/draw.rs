use super::*;

use wasm_boilerplate::*;

use std::f32::consts::PI;

impl<'a> super::Overlay<'a> {
    #[inline]
    pub fn plot_pixel(&mut self, c: Coord<i32>, pix: Rgba) {
        let offset = (c.y * self.dim.x as i32) + c.x;

        if offset >= (self.dim.x as i32 * self.dim.y as i32) || offset < 0 {
            unreachable!();
        }

        self.scene[offset as usize] = pix;
    }

    pub fn draw_arrow(&mut self, c: Coord<i32>, angle: f32, map_radius: f32) {
        let l1 = Coord {
            x: c.x + ((angle + PI * 3. / 4.0).cos() * map_radius * Self::ARROW_RATIO) as i32,
            y: c.y + ((angle + PI * 3. / 4.0).sin() * map_radius * Self::ARROW_RATIO) as i32,
        };

        let l2 = Coord {
            x: c.x + ((angle - PI * 3. / 4.0).cos() * map_radius * Self::ARROW_RATIO) as i32,
            y: c.y + ((angle - PI * 3. / 4.0).sin() * map_radius * Self::ARROW_RATIO) as i32,
        };

        let l3 = Coord {
            x: c.x + (angle.cos() * map_radius * Self::LONG_ARROW_RATIO) as i32,
            y: c.y + (angle.sin() * map_radius * Self::LONG_ARROW_RATIO) as i32,
        };

        self.draw_line(Line {
            p1: l1,
            p2: l2,
            b_pix: Self::RED,
            f_pix: Self::RED,
            ..Default::default()
        });
        self.draw_line(Line {
            p1: l2,
            p2: l3,
            b_pix: Self::GREEN,
            f_pix: Self::BLUE,
            ..Default::default()
        });
        self.draw_line(Line {
            p1: l1,
            p2: l3,
            b_pix: Self::RED,
            f_pix: Self::BLUE,
            ..Default::default()
        });
    }

    pub fn draw_box(&mut self, p1: Coord<i32>, p2: Coord<i32>, pix: Rgba) {
        let mut line = Line::default();

        line.b_pix = pix;
        line.f_pix = pix;

        line.p1 = p1;
        line.p2.y = p1.y;
        line.p2.x = p2.x;
        self.draw_line(line);

        line.p1 = p2;
        self.draw_line(line);

        line.p2.x = p1.x;
        line.p2.y = p2.y;
        self.draw_line(line);

        line.p1 = p1;
        self.draw_line(line);
    }

    pub fn draw_circle(&mut self, position: Coord<i32>, radius: f32, color: Rgba) {
        let mut location = Coord::default();
        let mut x = -radius - 1.0;

        while x <= radius {
            let y = (radius * radius - x * x).sqrt();

            location.x = x as i32 + position.x;
            location.y = y.floor() as i32 + position.y;
            self.plot_pixel(location, color);

            location.y = -y.floor() as i32 + position.y;
            self.plot_pixel(location, color);

            x += 1.0;
        }

        let mut y = -radius - 1.0;

        while y <= radius {
            let x = (radius * radius - y * y).sqrt();

            location.y = y as i32 + position.y;
            location.x = x.floor() as i32 + position.x;
            self.plot_pixel(location, color);

            location.x = -x.floor() as i32 + position.x;
            self.plot_pixel(location, color);

            y += 1.0;
        }
    }

    #[allow(dead_code)]
    pub fn fill_box(&mut self, p1: Coord<i32>, p2: Coord<i32>, pix: Rgba) {
        let mut line = Line::default();

        line.b_pix = pix;
        line.f_pix = pix;

        let mut i = p1.y;

        while i <= p2.y {
            line.p1.y = i;
            line.p2.y = i;
            line.p1.x = p1.x;
            line.p2.x = p2.x;

            self.draw_line(line);
            i += 1;
        }
    }
}
