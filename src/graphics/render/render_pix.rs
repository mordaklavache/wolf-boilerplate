// TODO : Contains High cost checks. Must be fixed one day!
#[cfg(debug_assertions)]
use wasm_boilerplate::log;
use wasm_boilerplate::{Coord, Rgba};

pub struct RenderPix<'a> {
    image: &'a [Rgba],
    dim: &'a Coord<usize>,
    bilinear_interpolation: bool,
    shadow_limit: f32,
}

impl<'a> RenderPix<'a> {
    #[inline]
    pub fn new(
        image: &'a [Rgba],
        dim: &'a Coord<usize>,
        bilinear_interpolation: bool,
        shadow_limit: f32,
    ) -> Self {
        Self {
            image,
            dim,
            bilinear_interpolation,
            shadow_limit,
        }
    }

    #[inline]
    pub fn get_pix(&self, c_src: Coord<f32>, dist: f32) -> Rgba {
        let mut pix = if self.bilinear_interpolation && dist < 3. {
            self.get_pix_complex(c_src)
        } else {
            self.get_pix_simple_checked(c_src)
        };
        if pix.red == 0xff && pix.blue == 0xff && pix.green == 0x00 {
            // Alpha 0 for purple part (sprites)
            pix.alpha = 0;
        } else if dist > self.shadow_limit {
            // Decrease color visibility if far
            let ratio_dist = self.shadow_limit / dist;
            pix.red = (pix.red as f32 * ratio_dist) as _;
            pix.green = (pix.green as f32 * ratio_dist) as _;
            pix.blue = (pix.blue as f32 * ratio_dist) as _;
        }
        pix
    }

    #[rustfmt::skip]
    #[inline]
    fn get_pix_complex(&self, float_src: Coord<f32>) -> Rgba {
        let integer_src = Coord {
            x: float_src.x as usize,
            y: float_src.y as usize,
        };

        if integer_src.x >= self.dim.x - 1 || integer_src.y >= self.dim.y - 1 {
            if integer_src.x > self.dim.x - 1 || integer_src.y > self.dim.y {
                #[cfg(debug_assertions)] {
                    log::error!("Pixel out of bound ! {:?}", integer_src);
                    unreachable!();
                }
                Rgba::default()
            } else {
                // Can't interpolate if the pixel is in the borders right or on bottom
                // console_log!("X/Y at end: {:?} / {:?}", integer_src, self.dim);
                self.get_pix_simple(float_src)
            }
        } else {
            let idx = integer_src.y * self.dim.x + integer_src.x;

            let corners: [Rgba; 4] = [
                self.image[idx],
                self.image[idx + 1],
                self.image[idx + self.dim.x],
                self.image[idx + self.dim.x + 1]];

            let delta = Coord::new(float_src.x.fract(), float_src.y.fract());
            #[cfg(debug_assertions)] {
                assert!(delta.x >= 0. && delta.x < 1.);
                assert!(delta.y >= 0. && delta.y < 1.);
            }

            let red = ((corners[0].red as f32 * (1. - delta.x) + corners[1].red as f32 * delta.x)
                * (1. - delta.y)
                + (corners[2].red as f32 * (1. - delta.x) + corners[3].red as f32 * delta.x)
                * delta.y)
                .round();
            let green = ((corners[0].green as f32 * (1. - delta.x) + corners[1].green as f32 * delta.x)
                * (1. - delta.y)
                + (corners[2].green as f32 * (1. - delta.x) + corners[3].green as f32 * delta.x)
                * delta.y)
                .round();
            let blue = ((corners[0].blue as f32 * (1. - delta.x) + corners[1].blue as f32 * delta.x)
                * (1. - delta.y)
                + (corners[2].blue as f32 * (1. - delta.x) + corners[3].blue as f32 * delta.x)
                * delta.y)
                .round();
            #[cfg(debug_assertions)] {
                assert!(red >= 0. && red <= 255.);
                assert!(green >= 0. && green <= 255.);
                assert!(blue >= 0. && blue <= 255.);
            }

            Rgba {
                red: red as u8,
                blue: blue as u8,
                green: green as u8,
                ..Default::default()
            }
        }
    }

    #[inline]
    fn get_pix_simple(&self, c_src: Coord<f32>) -> Rgba {
        let c_src_i = Coord::new(c_src.x as i32, c_src.y as i32);
        let i = self.dim.x as i32 * c_src_i.y + c_src_i.x;
        self.image[i as usize]
    }

    #[inline]
    fn get_pix_simple_checked(&self, c_src: Coord<f32>) -> Rgba {
        let c_src_i = Coord::new(c_src.x as i32, c_src.y as i32);
        let i = self.dim.x as i32 * c_src_i.y + c_src_i.x;
        if i < 0 || i >= self.dim.surface() as i32 {
            #[cfg(debug_assertions)]
            {
                log::error!("Pixel out of bound ! src={:?} i={}", c_src, i);
                unreachable!();
            }
            Rgba::default()
        } else {
            self.image[i as usize]
        }
    }
}

// let delta = Coord::new(
//     float_src.x - integer_src.x as f32,
//     float_src.y - integer_src.y as f32,
// );

// interp_rgba(
//     interp_rgba(corners[0], corners[1], delta.x),
//     interp_rgba(corners[2], corners[3], delta.x),
//     delta.y,
// )

// #[inline]
// fn interp_rgba(b: Rgba, f: Rgba, ratio: f32) -> Rgba {
//     assert!(ratio >= 0. && ratio < 1.);
//     Rgba {
//         blue: ((ratio * (f.blue as i16 - b.blue as i16) as f32) as u8) + b.blue,
//         green: ((ratio * (f.green as i16 - b.green as i16) as f32) as u8) + b.green,
//         red: ((ratio * (f.red as i16 - b.red as i16) as f32) as u8) + b.red,
//         ..Default::default() // TODO -> Not sure, maybe 0
//     }
// }

// With old C optim per variants (with error wien casting to u8 before adding other)
// 3 *
// 3 -
// 3 +
// 12 casts

// Without optim per variants
// 3 +
// 3 -
// 6 *
// 5 casts
// 1 round()
