use super::Render;

use wasm_boilerplate::{Coord, Rgba};

pub fn create_skybox(src: &[u8], src_dim: Coord<usize>, dst_dim: Coord<usize>) -> Vec<Rgba> {
    let ratio = (2. * std::f32::consts::PI / Render::VIEW_ANGLE) as usize;

    let sky_width = dst_dim.x * (ratio + 1);
    let mut sky_box: Vec<Rgba> = Vec::with_capacity(sky_width * dst_dim.y);
    unsafe {
        sky_box.set_len(sky_width * dst_dim.y);
    }
    super::transpose_image(
        &src,
        &mut sky_box,
        src_dim,
        Coord::new(dst_dim.x * ratio, dst_dim.y),
        Some(sky_width),
    );

    // paste bout code
    for y in 0..dst_dim.y {
        for x in 0..dst_dim.x {
            sky_box[y * sky_width + x + ratio * dst_dim.x] = sky_box[y * sky_width + x];
        }
    }
    sky_box
}
