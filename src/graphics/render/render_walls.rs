use crate::Player;

use wasm_boilerplate::{Coord, Rgba};

impl super::Render {
    pub fn mt_render_wall(&self, chunk: &mut [Rgba], player: Player, c: Coord<i32>) {
        let cl = &self.calc.columns[c.x as usize];

        let (image, dim) = unsafe { self.walls.get_unchecked(cl.wall.kind as usize) };

        let mut uv = Coord {
            y: (player.height + cl.wall_h_dist * self.calc.atan_list[c.y as usize])
                / cl.wall.height,
            x: cl.wall_x_offset + -cl.wall_x_tex / 4.0 * (dim.x - 8) as f32,
        };
        uv.y = (1.0 - uv.y) * (dim.y - 1) as f32;

        let dist = cl.wall_h_dist;

        let pix = super::render_pix::RenderPix::new(
            image,
            dim,
            self.bilinear_interpolation.0,
            self.shadow_limit,
        )
        .get_pix(uv, dist);
        chunk[c.x as usize] = pix;
    }

    // NO MULTITHREADED CODE FOR BACKWARD COMPATIBILITY
    pub fn render_wall(&self, scene: &mut [Rgba], player: Player, c: Coord<i32>) {
        let cl = &self.calc.columns[c.x as usize];

        let (image, dim) = unsafe { self.walls.get_unchecked(cl.wall.kind as usize) };

        let mut uv = Coord {
            y: (player.height + cl.wall_h_dist * self.calc.atan_list[c.y as usize])
                / cl.wall.height,
            x: cl.wall_x_offset + -cl.wall_x_tex / 4.0 * (dim.x - 8) as f32,
        };
        uv.y = (1.0 - uv.y) * (dim.y - 1) as f32;

        let dist = cl.wall_h_dist;

        let pix = super::render_pix::RenderPix::new(
            image,
            dim,
            self.bilinear_interpolation.0,
            self.shadow_limit,
        )
        .get_pix(uv, dist);
        scene[c.y as usize * self.dim.x + c.x as usize] = pix;
    }
}
