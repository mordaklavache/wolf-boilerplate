use super::Columns;
use crate::{Elem, Player};

use wasm_boilerplate::Coord;

use std::cmp::Ordering;
use std::ops::Index;

struct WallFinder<'a, T, U>
where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    map: &'a T,
    player_location: Coord<f32>,

    ray_dir: Coord<f32>,
    c_i: Coord<i32>,
    d_dist: Coord<f32>,
    side_dist: Coord<f32>,
    step: Coord<i32>,
}

impl<'a, T, U> WallFinder<'a, T, U>
where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    fn new(map: &'a T, player_location: Coord<f32>, angle_x: f32) -> Self {
        let ray_dir = Coord {
            x: angle_x.cos(),
            y: angle_x.sin(),
        };

        let c_i = Coord {
            x: player_location.x as i32,
            y: player_location.y as i32,
        };

        let d_dist = Coord {
            x: match ray_dir.x.total_cmp(&0.0) {
                Ordering::Equal => 0.0,
                _ => 1.0 / ray_dir.x.abs(),
            },
            y: match ray_dir.y.total_cmp(&0.0) {
                Ordering::Equal => 0.0,
                _ => 1.0 / ray_dir.y.abs(),
            },
        };

        let (step, side_dist) = {
            let (step_x, side_dist_x) = match ray_dir.x.total_cmp(&0.0) {
                Ordering::Less => (-1, (player_location.x - c_i.x as f32) * d_dist.x),
                Ordering::Greater => (1, (c_i.x as f32 + 1.0 - player_location.x) * d_dist.x),
                Ordering::Equal => (0, 0.0),
            };
            let (step_y, side_dist_y) = match ray_dir.y.total_cmp(&0.0) {
                Ordering::Less => (-1, (player_location.y - c_i.y as f32) * d_dist.y),
                Ordering::Greater => (1, (c_i.y as f32 + 1.0 - player_location.y) * d_dist.y),
                Ordering::Equal => (0, 0.0),
            };
            (
                Coord::new(step_x, step_y),
                Coord::new(side_dist_x, side_dist_y),
            )
        };

        Self {
            map,
            player_location,
            ray_dir,
            c_i,
            d_dist,
            side_dist,
            step,
        }
    }

    fn exec(&mut self) -> i32 {
        loop {
            let side = if self.ray_dir.y == 0.0 || self.side_dist.x < self.side_dist.y {
                self.side_dist.x += self.d_dist.x;
                self.c_i.x += self.step.x;
                0
            } else {
                self.side_dist.y += self.d_dist.y;
                self.c_i.y += self.step.y;
                1
            };

            if let Elem::Wall(_) = self.map[self.c_i.y as usize][self.c_i.x as usize] {
                break (side);
            }
        }
    }

    // This function has been modified for the new wolf3d subject (circa late 2017)
    // x_tex is now in the range [0, 4[ instead of [0, 1[
    // Meanwhile, wall textures are now 4 times larger, the new range binding to
    // the four sides of the wall blocks.
    fn intersect(&self, side: i32) -> (f32, Coord<f32>) {
        let wall_dist = if side == 0 {
            ((self.c_i.x as f32) - self.player_location.x + (1.0 - self.step.x as f32) / 2.0)
                / self.ray_dir.x
        } else {
            ((self.c_i.y as f32) - self.player_location.y + (1.0 - self.step.y as f32) / 2.0)
                / self.ray_dir.y
        };

        let intersect = Coord {
            x: self.player_location.x + wall_dist as f32 * self.ray_dir.x,
            y: self.player_location.y + wall_dist as f32 * self.ray_dir.y,
        };

        let mut x_tex = -1.;
        if side == 1 {
            x_tex += (intersect.x - self.c_i.x as f32) * self.step.y as f32;
            if (self.c_i.y as f32) >= intersect.y - 0.5 {
                x_tex -= 3.0;
            }
        } else {
            x_tex += (1.0 - intersect.y + self.c_i.y as f32) * self.step.x as f32;
            if (self.c_i.x as f32) < intersect.x - 0.5 {
                x_tex -= 1.0;
            }
        }
        (x_tex, intersect)
    }
}

// This function finds the intersection between a ray and the first wall
// it encounters, along with the x uv coordinate on the texture
//   inputs:
//     map: 2d array representing the map (value >= 50 == wall)
//     player: location of the players
//     angle_x: angle of the ray
//   outputs:
//     intersect: x, y coordinates of the intersection of the ray with the wall
//     x_tex: x uv coordinate of the ray on the texture
pub fn find_wall<'a, T, U>(
    column: &mut Columns,
    map: &T,
    player: &Player,
    angle_x: f32,
    intersect: &mut Coord<f32>,
) where
    T: Index<usize, Output = U>,
    U: Index<usize, Output = Elem>,
{
    let mut wall_finder = WallFinder::new(map, player.location, angle_x);

    let side = wall_finder.exec();
    (column.wall_x_tex, *intersect) = wall_finder.intersect(side);
    column.wall = match map[wall_finder.c_i.y as usize][wall_finder.c_i.x as usize] {
        Elem::Wall(wall) => wall,
        _others => panic!("not a wall!"),
    };
    column.wall_x_offset = (-column.wall_x_tex).floor() * 2. + 1.; // x-offset on 4X images
}
