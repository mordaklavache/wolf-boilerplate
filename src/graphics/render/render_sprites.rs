// sub overflow:
// SpriteEnv {
//     angle_top_left: Coord { x: -1.5707963, y: -0.394791 },
//     angle_bottom_right: Coord { x: 2.3841858e-7, y: -1.2277725 },
//     c: Coord { x: 0, y: 626 },
//     c_top_left: Coord { x: -2147483008, y: 626 },
//     c_bottom_right: Coord { x: 640, y: 2152 },
//     c_max: Coord { x: 640, y: 719 },
//     c_tex: Coord { x: 0.0, y: 0.0 }
// }
// Sprite {
//     map_location: Coord { x: 5, y: 15 },
//     kind: Dog, height: 1.2,
//     origin_location: Coord { x: 5.5, y: 15.5 },
//     current_location: Coord { x: 5.5, y: 15.5 },
//     target_location: Coord { x: 4.5, y: 15.5 },
//     last_time: Some(5182072.34),
//     dist: 0.49999997,
//     angle0_x: -0.785398
// }
// Player {
//     location: Coord { x: 6.0, y: 16.0 },
//     height: 1.4,
//     angle: 4.712389
// }.
use crate::{Player, Sprite};

use wasm_boilerplate::{Coord, Rgba};

use super::dist;

#[derive(Debug, Default)]
struct SpriteEnv {
    angle_top_left: Coord<f32>,
    angle_bottom_right: Coord<f32>,
    c: Coord<i32>,
    c_top_left: Coord<i32>,
    c_bottom_right: Coord<i32>,
    c_max: Coord<i32>,
    c_tex: Coord<f32>,
}

impl super::Render {
    pub fn render_sprites(&self, scene: &mut [Rgba], player: Player, sprites: &mut [Sprite]) {
        self.create_z_buffer_order(&player, sprites);
        for sprite in sprites {
            if sprite.angle0_x < 0.0 {
                sprite.angle0_x += 2.0 * std::f32::consts::PI;
            }

            if sprite.angle0_x.cos() <= 0.0 {
                continue;
            }
            self.draw_sprite(scene, &player, sprite);
        }
    }

    fn create_z_buffer_order(&self, player: &Player, sprites: &mut [Sprite]) {
        for sprite in &mut *sprites {
            sprite.angle0_x = (sprite.current_location.y - player.location.y)
                .atan2(sprite.current_location.x - player.location.x)
                - player.angle;

            // // TODO: Ask to Steven
            // // Debug view of the following problem of tan(x) = infinite
            // if sprite.angle0_x == -1.5 * std::f32::consts::PI {
            //     use wasm_bindgen::prelude::*;
            //     use wasm_boilerplate::console_log;

            //     console_log!("error !");
            //     console_log!("1 - {}", player.angle);
            //     console_log!(
            //         "2 - {} - {:?} {:?}",
            //         (sprite.location.y - player.location.y)
            //             .atan2(sprite.location.x - player.location.x),
            //         sprite.location,
            //         player.location
            //     );
            //     console_log!(
            //         "3 - {} {} -> {}",
            //         sprite.angle0_x,
            //         self.dim.x,
            //         Self::angle_to_pix(sprite.angle0_x, self.dim.x as f32)
            //     );
            // }
            let angle_to_pix = Self::angle_to_pix(sprite.angle0_x, self.dim.x as f32);

            // FIX overflow when addition `angle_to_pix + self.dim.x / 2`
            // WHEN angle_to_pix give high number because tan(x) is nearly infinite like (-S3/2PI)
            let mut l = if angle_to_pix > self.dim.x as i32 / 2 {
                0
            } else {
                angle_to_pix + self.dim.x as i32 / 2
            };
            if l < 0 {
                l = 0;
            } else if l >= self.dim.x as i32 {
                l = self.dim.x as i32 - 1;
            }
            sprite.dist =
                dist(player.location, sprite.current_location) * self.calc.cos_list[l as usize];
        }
        sprites.sort_by(|a, b| b.partial_cmp(a).unwrap());
    }

    fn draw_sprite(&self, scene: &mut [Rgba], player: &Player, sprite: &Sprite) {
        let mut sprite_env = SpriteEnv::default();
        sprite_env.angle_top_left.x = sprite.angle0_x - (0.5 / sprite.dist).atan();
        sprite_env.angle_bottom_right.x = sprite.angle0_x + (0.5 / sprite.dist).atan();
        sprite_env.c_top_left.x =
            Self::angle_to_pix(sprite_env.angle_top_left.x, self.dim.x as f32)
                + self.dim.x as i32 / 2;
        sprite_env.c_bottom_right.x =
            Self::angle_to_pix(sprite_env.angle_bottom_right.x, self.dim.x as f32)
                + self.dim.x as i32 / 2;

        sprite_env.angle_top_left.y = (sprite.height - player.height).atan() / sprite.dist;
        sprite_env.angle_bottom_right.y = (-player.height / sprite.dist).atan();
        sprite_env.c_top_left.y = self.dim.y as i32 / 2
            - Self::angle_to_pix(sprite_env.angle_top_left.y, self.dim.x as f32);
        sprite_env.c_bottom_right.y = self.dim.y as i32 / 2
            - Self::angle_to_pix(sprite_env.angle_bottom_right.y, self.dim.x as f32);

        sprite_env.c.x = sprite_env.c_top_left.x.max(0);
        sprite_env.c_max.x = sprite_env.c_bottom_right.x.min(self.dim.x as i32 - 1);

        sprite_env.c.y = sprite_env.c_top_left.y.max(0);
        sprite_env.c_max.y = sprite_env.c_bottom_right.y.min(self.dim.y as i32 - 1);

        let (image, dim) = unsafe { self.sprites.get_unchecked(sprite.kind as usize) };
        for x in sprite_env.c.x..sprite_env.c_max.x {
            if sprite.dist > self.calc.columns[x as usize].wall_h_dist {
                continue;
            }
            for y in sprite_env.c.y..sprite_env.c_max.y {
                #[cfg(debug_assertions)]
                {
                    // TODO : Remove that after finding the bug.
                    use wasm_bindgen::prelude::*;
                    use wasm_boilerplate::console_log;

                    if let None = sprite_env
                        .c_bottom_right
                        .x
                        .checked_sub(sprite_env.c_top_left.x)
                    {
                        console_log!(
                            "sub overflow: {:?} for sprite {:?} player at {:?}.",
                            sprite_env,
                            sprite,
                            player
                        );
                    }
                    if let None = sprite_env
                        .c_bottom_right
                        .y
                        .checked_sub(sprite_env.c_top_left.y)
                    {
                        console_log!(
                            "sub overflow: {:?} for sprite {:?} player at {:?}.",
                            sprite_env,
                            sprite,
                            player
                        );
                    }
                }
                sprite_env.c_tex.x = (x - sprite_env.c_top_left.x) as f32
                    / (sprite_env.c_bottom_right.x - sprite_env.c_top_left.x) as f32 // TODO: BUGFIX NEEDED - Substract overflow
                    * (dim.x as f32 - 2.);

                sprite_env.c_tex.y = (y - sprite_env.c_top_left.y) as f32
                    / (sprite_env.c_bottom_right.y - sprite_env.c_top_left.y) as f32
                    * (dim.y as f32 - 2.);

                let pix = super::render_pix::RenderPix::new(image, dim, false, self.shadow_limit)
                    .get_pix(sprite_env.c_tex, sprite.dist);

                if pix.alpha != 0x00 {
                    scene[y as usize * self.dim.x + x as usize] = pix;
                }
            }
        }
    }

    fn angle_to_pix(angle: f32, dim_x: f32) -> i32 {
        // use wasm_bindgen::prelude::*;
        // use wasm_boilerplate::console_log;
        // console_log!("tangeante angle {} = {}", angle, angle.tan());
        (angle.tan() / (Self::VIEW_ANGLE / 2.0).tan() * (dim_x / 2.)) as i32
    }
}
