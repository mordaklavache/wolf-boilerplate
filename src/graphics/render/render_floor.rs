// TODO - A bug is maybe present in this file.
use crate::{Elem, Player};

use wasm_boilerplate::{Coord, Rgba};

use std::ops::Index;

impl super::Render {
    pub fn mt_render_floor<T, U>(
        &self,
        chunk: &mut [Rgba],
        player: Player,
        map: &T,
        pix_coord: Coord<i32>,
        angle: Coord<f32>,
    ) where
        T: Index<usize, Output = U>,
        U: Index<usize, Output = Elem>,
    {
        let dist =
            self.calc.dist_floor[pix_coord.y as usize] / self.calc.cos_list[pix_coord.x as usize];

        let mut uv = Coord {
            x: (player.location.x + dist * angle.x.cos()),
            y: (player.location.y + dist * angle.x.sin()),
        };

        let floor = match map[uv.y as usize][uv.x as usize] {
            Elem::Floor(floor) => floor,
            _others => return,
            // Sometimes it is a Wall !!! That Bug were present on legacy wolf...
            // others => {
            //     use wasm_boilerplate::console_log;
            //     use wasm_bindgen::prelude::*;
            //     use crate::FloorKind;

            //     console_log!("{:?} at X: {} Y: {}", others, map_y, map_x);
            //     return;
            // //    FloorKind::Carpet
            // }
        };
        let (image, dim) = unsafe { self.floors.get_unchecked(floor as usize) };

        uv.x *= 0.25; // Used to KEEP old Steven's floor style.
        uv.y *= 0.25;

        uv.x = (uv.x.fract()) * (dim.x - 1) as f32;
        uv.y = (uv.y.fract()) * (dim.y - 1) as f32;

        let pix = super::render_pix::RenderPix::new(
            image,
            dim,
            self.bilinear_interpolation.0,
            self.shadow_limit,
        )
        .get_pix(uv, dist);
        chunk[pix_coord.x as usize] = pix;
    }

    // NO MULTITHREADED CODE FOR BACKWARD COMPATIBILITY
    pub fn render_floor<T, U>(
        &self,
        scene: &mut [Rgba],
        player: Player,
        map: &T,
        pix_coord: Coord<i32>,
        angle: Coord<f32>,
    ) where
        T: Index<usize, Output = U>,
        U: Index<usize, Output = Elem>,
    {
        let dist =
            self.calc.dist_floor[pix_coord.y as usize] / self.calc.cos_list[pix_coord.x as usize];

        let mut uv = Coord {
            x: (player.location.x + dist * angle.x.cos()),
            y: (player.location.y + dist * angle.x.sin()),
        };

        let floor = match map[uv.y as usize][uv.x as usize] {
            Elem::Floor(floor) => floor,
            _others => return,
        };
        let (image, dim) = unsafe { self.floors.get_unchecked(floor as usize) };

        uv.x *= 0.25;
        uv.y *= 0.25;

        uv.x = (uv.x.fract()) * (dim.x - 1) as f32;
        uv.y = (uv.y.fract()) * (dim.y - 1) as f32;

        let pix = super::render_pix::RenderPix::new(
            image,
            dim,
            self.bilinear_interpolation.0,
            self.shadow_limit,
        )
        .get_pix(uv, dist);
        scene[pix_coord.y as usize * self.dim.x + pix_coord.x as usize] = pix;
    }
}
