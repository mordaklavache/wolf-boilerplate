mod draw;
mod draw_line;
mod minimap;

use wasm_boilerplate::*;

const MAP_DEPTH: i32 = 30;

#[derive(Default, Debug, Copy, Clone)]
pub struct Line {
    p1: Coord<i32>,
    p2: Coord<i32>,
    d: Coord<i32>,
    b_pix: Rgba,
    f_pix: Rgba,
}

pub struct Overlay<'a> {
    scene: &'a mut [Rgba],
    dim: &'a Coord<usize>,
}

impl<'a> Overlay<'a> {
    const RADAR_TURN_DURATION_MS: f64 = 3000.;

    pub const WHITE: Rgba = Rgba {
        red: 255,
        green: 255,
        blue: 255,
        alpha: 255,
    };
    pub const RED: Rgba = Rgba {
        red: 255,
        green: 0,
        blue: 0,
        alpha: 255,
    };
    pub const GREEN: Rgba = Rgba {
        red: 0,
        green: 255,
        blue: 0,
        alpha: 255,
    };
    pub const BLUE: Rgba = Rgba {
        red: 0,
        green: 0,
        blue: 255,
        alpha: 255,
    };

    pub fn new(scene: &'a mut [Rgba], dim: &'a Coord<usize>) -> Self {
        Self { scene, dim }
    }
}
