use crate::*;

pub fn parse(map: &str) -> (Vec<Vec<Elem>>, Player, Vec<Sprite>) {
    let mut player_location = None;
    let mut sprites = Vec::new();

    let map: Vec<Vec<Elem>> = map
        .split(|c| c == '\n')
        .filter(|line| line.len() != 0)
        .enumerate()
        .map(|(idx_line, line)| {
            console_log!("{}", line);
            line.split(|c| c == ' ')
                .enumerate()
                .map(|(idx_row, val)| {
                    assert_eq!(val.len(), 2); // check if it is a group of 2 characters

                    let location = Coord {
                        x: idx_row as f32,
                        y: idx_line as f32,
                    };
                    use SpriteKind::*;
                    match &val[1..] {
                        "_" => {}
                        "%" => {
                            assert_eq!(player_location, None); // Only one player location should exists
                            player_location = Some(location);
                        }
                        "0" => sprites.push(Sprite {
                            map_location: location.into(),
                            kind: Pig,
                            height: 1.9,
                            ..Default::default()
                        }),
                        "1" => sprites.push(Sprite {
                            map_location: location.into(),
                            kind: Dog,
                            ..Default::default()
                        }),
                        "2" => sprites.push(Sprite {
                            map_location: location.into(),
                            kind: Sadirak,
                            height: 3.5,
                            ..Default::default()
                        }),
                        _ => unreachable!(), // Unhandled sprite
                    }
                    use {FloorKind::*, WallKind::*};
                    match &val[0..1] {
                        "a" => Elem::Wall(Wall {
                            kind: FourWall,
                            ..Default::default()
                        }),
                        "b" => Elem::Wall(Wall {
                            kind: FourWall2,
                            ..Default::default()
                        }),
                        "c" => Elem::Wall(Wall {
                            kind: Piggie,
                            ..Default::default()
                        }),
                        "d" => Elem::Wall(Wall {
                            kind: Aperture,
                            ..Default::default()
                        }),
                        "e" => Elem::Wall(Wall {
                            kind: Panic,
                            ..Default::default()
                        }),
                        "_" => Elem::Floor(Parquet),
                        "1" => Elem::Floor(Carpet),
                        "2" => Elem::Floor(Stone),
                        _ => unreachable!(), // Unhandled Element
                    }
                })
                .collect()
        })
        .collect();
    assert_ne!(player_location, None); // check if player location exists

    // check is all line have the same row number
    for i in 0..map.len() {
        assert_eq!(map[0].len(), map[i].len());
    }
    (
        map,
        Player {
            location: player_location.unwrap(),
            ..Default::default()
        },
        sprites,
    )
}
