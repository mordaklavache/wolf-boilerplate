rm -v Cargo.lock
rm -r pkg
cargo clean
cargo clean --manifest-path dependencies/wasm-boilerplate/Cargo.toml
echo "Cleaning complete!"
