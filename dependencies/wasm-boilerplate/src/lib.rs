//!This library allows to handle calls to the DOM
#![deny(missing_docs)]
#![feature(result_flattening)]

mod file;
pub mod png_reader;
pub mod song;
pub mod vbe_font;
#[macro_use]
mod console;

use file::File;
use js_sys::Promise;
use js_sys::Uint8ClampedArray;
use js_sys::WebAssembly;
pub use log;

use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::JsFuture;
use web_sys::DeviceMotionEvent;
use web_sys::OrientationType;
use web_sys::Performance;
use web_sys::{CanvasRenderingContext2d, EventTarget, KeyboardEvent};

use std::cell::RefCell;
use std::ffi::CString;
use std::ffi::OsStr;
use std::path::Path;
use std::ptr::slice_from_raw_parts;
use std::rc::Rc;

/// This structure represents the canvas.
pub struct GraphicContext {
    /// the width of the canvas
    pub width: usize,
    /// the height of the canvas
    pub height: usize,
    context: CanvasRenderingContext2d,
    js_array: Uint8ClampedArray, // Preallocated JS no-shared data
    messages: Vec<String>,
}

/// 32bits RGBA pixel
#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
#[repr(packed)]
pub struct Rgba {
    /// red variant
    pub red: u8,
    /// green variant
    pub green: u8,
    /// blue variant
    pub blue: u8,
    /// alpha variant
    pub alpha: u8,
}

impl Default for Rgba {
    fn default() -> Self {
        Self {
            red: 0,
            green: 0,
            blue: 0,
            alpha: 255,
        }
    }
}

macro_rules! resize_routine {
    ($canvas:expr) => {
        let window_witdh: f64 = window().inner_width().unwrap().as_f64().unwrap();
        let window_height: f64 = window().inner_height().unwrap().as_f64().unwrap();
        let canvas_width = $canvas.width() as f64;
        let canvas_height = $canvas.height() as f64;
        if window_witdh / window_height < canvas_width / canvas_height {
            $canvas.class_list().replace("height", "width").unwrap();
        } else {
            $canvas.class_list().replace("width", "height").unwrap();
        }
    };
}

impl GraphicContext {
    /// Create a new GraphicContext.
    pub fn new() -> Result<Self, JsValue> {
        wasm_logger::init(wasm_logger::Config::default());
        std::panic::set_hook(Box::new(console_error_panic_hook::hook));
        log::info!("Log is ready");
        log::warn!("This website is backed by rust nightly with recompiled STD!");

        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();

        let canvas: web_sys::HtmlCanvasElement = canvas.dyn_into::<web_sys::HtmlCanvasElement>()?;
        let canvas_clone = canvas.clone();

        resize_routine!(canvas);
        let closure: Closure<dyn FnMut()> = Closure::new(move || -> () {
            resize_routine!(canvas_clone);
        });

        let event_target: EventTarget = window().into();
        event_target
            .add_event_listener_with_callback("resize", closure.as_ref().unchecked_ref())
            .unwrap();
        closure.forget();

        let context: CanvasRenderingContext2d = canvas
            .get_context("2d")?
            .unwrap()
            .dyn_into::<CanvasRenderingContext2d>()?;

        let win = Coord {
            x: canvas.width() as usize,
            y: canvas.height() as usize,
        };

        let frame_buffer = vec![Rgba::default(); win.surface()];
        let base = frame_buffer.as_ptr() as u32;
        let len = frame_buffer.len() * 4;
        // Use the raw access available through `memory.buffer`, but be sure to
        // use `slice` instead of `subarray` to create a copy that isn't backed
        // by `SharedArrayBuffer`. Currently `ImageData` rejects a view of
        // `Uint8ClampedArray` that's backed by a shared buffer.
        let mem = wasm_bindgen::memory().unchecked_into::<WebAssembly::Memory>();
        // Constructs the Uint8ClampedArray from no-shared memory.
        let js_array = Uint8ClampedArray::new(&mem.buffer()).slice(base, base + len as u32);

        Ok(Self {
            context,
            width: win.x,
            height: win.y,
            js_array,
            messages: Vec::new(),
        })
    }

    /// Registers a rust closure that will be called on each frame refresh.
    pub fn loop_hook<'a, F>(mut self, mut c: F) -> Result<f64, JsValue>
    where
        F: FnMut(&mut Uint8ClampedArray, f64) -> bool + 'static,
    {
        let f = Rc::new(RefCell::new(None));
        let g = f.clone();
        let mut delta = 0;
        *g.borrow_mut() = Some(Closure::new(move |timestamp: f64| {
            if delta % 1 == 0 && c(&mut self.js_array, timestamp) {
                let image_data =
                    ImageData::new(&self.js_array, self.width as f64, self.height as f64).unwrap();
                let image_data: &web_sys::ImageData = unsafe { std::mem::transmute(&image_data) };
                self.context.put_image_data(&image_data, 0.0, 0.0).unwrap();
            }
            delta += 1;
            request_animation_frame(f.borrow().as_ref().unwrap());
        }));
        request_animation_frame(g.borrow().as_ref().unwrap());
        Ok(performance().now())
    }

    /// Gets the dimensions of the canvas.
    pub fn get_dimensions(&self) -> Coord<usize> {
        Coord {
            x: self.context.canvas().unwrap().width() as usize,
            y: self.context.canvas().unwrap().height() as usize,
        }
    }

    /// Display raw String into screen
    pub fn raw_string<T: ToString>(&mut self, string: T) {
        // TODO : Only first N cols can be displayed.
        self.messages.push(string.to_string());
        let cols = self.height / vbe_font::CHAR_HEIGHT - 2;

        let mut frame_buffer = vec![
            Rgba {
                // C64 blue color
                green: 58,
                blue: 255,
                ..Default::default()
            };
            self.width * self.height
        ];
        for (index, string) in self.messages.iter().enumerate() {
            if index == cols {
                break;
            }
            unsafe {
                vbe_font::display_string(
                    &mut frame_buffer,
                    CString::new(string.as_bytes()).unwrap(),
                    Coord {
                        x: vbe_font::CHAR_HEIGHT,
                        y: index * vbe_font::CHAR_HEIGHT + vbe_font::CHAR_HEIGHT,
                    },
                    Coord {
                        x: self.width,
                        y: self.height,
                    },
                    Rgba {
                        red: 0xff,
                        green: 0xff,
                        blue: 0xff,
                        ..Default::default()
                    },
                );
            }
        }
        // Draw frame
        let u8_slice =
            { slice_from_raw_parts(frame_buffer.as_ptr() as *const u8, frame_buffer.len() * 4) };
        self.js_array
            .copy_from(unsafe { u8_slice.as_ref().unwrap() });
        let image_data =
            ImageData::new(&self.js_array, self.width as f64, self.height as f64).unwrap();
        let image_data: &web_sys::ImageData = unsafe { std::mem::transmute(&image_data) };
        self.context.put_image_data(&image_data, 0.0, 0.0).unwrap();
    }
}

// Inline the definition of `ImageData` here because `web_sys` uses
// `&Clamped<Vec<u8>>`, whereas we want to pass in a JS object here.
#[wasm_bindgen]
extern "C" {
    pub type ImageData;

    #[wasm_bindgen(constructor, catch)]
    fn new(data: &Uint8ClampedArray, width: f64, height: f64) -> Result<ImageData, JsValue>;
}

fn request_animation_frame(f: &Closure<dyn FnMut(f64)>) {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK");
}

/// Returns the web browser's performance API object.
///
/// This gives access to the browser's high resolution performance
/// metrics through [Performance](https://developer.mozilla.org/fr/docs/Web/Performance).
///
/// Can be used to access timing info, monitor performance, etc.  
pub fn performance() -> Performance {
    window()
        .performance()
        .expect("performance should be available")
}

/// Trait to register keyboard event handler methods.
///
/// Types that implement this trait can register functions to be called
/// on key press and key release events.
///
/// This allows abstracting away keyboard input handling.
pub trait Keyboard {
    /// Called when a key is pressed.
    fn keydown(&mut self);
    /// Called when a key is released.
    fn keyup(&mut self);
}

/// Device Orientation
#[derive(Copy, Clone, Default)]
pub struct MotionCoord {
    /// Orientation around X axis
    pub x: Option<f64>,
    /// Orientation around Y axis
    pub y: Option<f64>,
    /// Orientation around Z axis
    pub z: Option<f64>,
}

/// Keyboard and Orientation Register
#[derive(Copy, Clone)]
pub struct Register<T: Keyboard + 'static> {
    /// Keyboard Status
    pub keyboard: [T; 256],
    /// Acceleration including gravity current status
    pub acceleration_including_gravity: MotionCoord,
    /// Acceleration status
    pub acceleration: MotionCoord,
    /// Acceleration sum
    pub acceleration_sum: MotionCoord,
    /// Smartphone orientation
    pub orientation: Option<OrientationType>,
}

/// Registers all user events
/// Registers a key state array to update on keyboard events.
/// Registers accelerometers MotionEvents. (mobile)
/// Registers screen orientation changes. (mobile)
///
/// This takes an `Rc<RefCell<[T; 256]>>` which will be updated when keys are
/// pressed or released to represent the current keyboard state.
///
/// The array should have 256 elements representing each key code. The type `T`
/// must implement the `Keyboard` trait which handles updating the state on
/// key events.
///
/// This register will be automatically updated to reflect the key state.
pub fn set_user_events<T: Keyboard + 'static>(register: Rc<RefCell<Register<T>>>) {
    let document = window().document().unwrap();
    let event_target: EventTarget = document.into();

    let register_clone = register.clone();
    let register_clone2 = register.clone();
    let register_clone3 = register.clone();

    let closure = Closure::wrap(Box::new(move |event: KeyboardEvent| {
        assert!(event.key_code() < 256);
        // console_log!("KeyDown: {:?} code = {}", event.key(), event.key_code());
        register.borrow_mut().keyboard[event.key_code() as usize].keydown();
    }) as Box<dyn FnMut(_)>);

    event_target
        .add_event_listener_with_callback("keydown", closure.as_ref().unchecked_ref())
        .unwrap();
    closure.forget();

    let closure = Closure::wrap(Box::new(move |event: KeyboardEvent| {
        assert!(event.key_code() < 256);
        // console_log!("KeyUp: {:?} code = {}", event.key(), event.code());
        register_clone.borrow_mut().keyboard[event.key_code() as usize].keyup();
    }) as Box<dyn FnMut(_)>);

    event_target
        .add_event_listener_with_callback("keyup", closure.as_ref().unchecked_ref())
        .unwrap();
    closure.forget();

    let closure = Closure::wrap(Box::new(move |event: DeviceMotionEvent| {
        let mut borrow = register_clone2.borrow_mut();
        if let Some(acceleration) = event.acceleration_including_gravity() {
            borrow.acceleration_including_gravity = MotionCoord {
                x: acceleration.x(),
                y: acceleration.y(),
                z: acceleration.z(),
            };
        }
        if let Some(acceleration) = event.acceleration() {
            borrow.acceleration = MotionCoord {
                x: acceleration.x(),
                y: acceleration.y(),
                z: acceleration.z(),
            };
            borrow.acceleration_sum.x = borrow
                .acceleration_sum
                .x
                .map_or_else(|| acceleration.x(), |a| acceleration.x().map(|b| a + b));
            borrow.acceleration_sum.y = borrow
                .acceleration_sum
                .y
                .map_or_else(|| acceleration.y(), |a| acceleration.y().map(|b| a + b));
            borrow.acceleration_sum.z = borrow
                .acceleration_sum
                .z
                .map_or_else(|| acceleration.z(), |a| acceleration.z().map(|b| a + b));
        }
    }) as Box<dyn FnMut(_)>);

    window()
        .add_event_listener_with_callback("devicemotion", closure.as_ref().unchecked_ref())
        .unwrap();
    closure.forget();

    if let Ok(screen) = window().screen() {
        let closure = Closure::wrap(Box::new(move |event: web_sys::Event| {
            let mut borrow = register_clone3.borrow_mut();
            let orientation_event = event
                .target()
                .unwrap()
                .clone()
                .dyn_into::<web_sys::ScreenOrientation>()
                .unwrap();
            console_log!("orientation changed {:?}", orientation_event.type_());
            borrow.orientation = orientation_event.type_().ok();
        }) as Box<dyn FnMut(_)>);
        screen
            .orientation()
            .add_event_listener_with_callback("change", closure.as_ref().unchecked_ref())
            .unwrap();
        closure.forget();
    }
}

/// Get current screen orientation (moble)
pub fn get_screen_orientation_type() -> Option<web_sys::OrientationType> {
    window()
        .screen()
        .map(|screen| screen.orientation().type_())
        .flatten()
        .ok()
}

/// Fetch a ressource from a given URL and send it to closure.
pub async fn fetch_resource<F, E>(url: &str, c: F) -> Result<E, JsValue>
where
    F: FnOnce(Vec<u8>, &OsStr) -> E,
{
    let resp = JsFuture::from(fetch(url)).await?;
    let reponse: web_sys::Response = resp.into();
    let array_buffer = JsFuture::from(reponse.array_buffer()?).await?;
    let v: Vec<u8> = js_sys::Uint8Array::new(&array_buffer).to_vec();
    Ok(c(v, Path::new(url).file_name().unwrap()))
}

/// A generic coordinate type with x and y fields.
///
/// This represents a coordinate pair for some 2D position. The type `T` can be
/// any numeric type like u32 or f32.
///
/// Derives common traits like Debug, Copy, Clone etc. for convenience.
///
/// Can be used for positions, sizes, vectors etc.
#[derive(Debug, Copy, Clone, Default, PartialEq)]
pub struct Coord<T> {
    /// X component of the coordinate.
    pub x: T,
    /// Y component of the coordinate.
    pub y: T,
}
impl<T> Coord<T> {
    #[inline]
    /// Creates a new Coord with the given x and y values.
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: std::ops::Mul<Output = T> + Copy> Coord<T> {
    #[inline]
    /// Returns the surface area of the Coord as x * y.
    pub fn surface(&self) -> T {
        self.x * self.y
    }
}

impl From<Coord<f32>> for Coord<usize> {
    #[inline]
    fn from(f: Coord<f32>) -> Self {
        Self {
            x: f.x as usize,
            y: f.y as usize,
        }
    }
}

impl From<Coord<usize>> for Coord<f32> {
    #[inline]
    fn from(u: Coord<usize>) -> Self {
        Self {
            x: u.x as f32,
            y: u.y as f32,
        }
    }
}
#[wasm_bindgen]
extern "C" {
    fn fetch(input: &str) -> Promise;
}

fn window() -> web_sys::Window {
    web_sys::window().expect("no global `window` exists")
}
