/// Prints a message to the web browser's console log.
///
/// This macro provides quick access to logging diagnostics
/// messages in the browser console.
///
/// It works both in debug and release builds.
#[macro_export]
macro_rules! console_log {
    ($($t:tt)*) => ({
        #[wasm_bindgen]
        extern "C" {
            #[wasm_bindgen(js_namespace = console)]
            fn log(s: &str);
        }
        log(&format_args!($($t)*).to_string())
    })
}

/// Prints a message to the browser.
///
/// This macro acts like the `dbg!` macro in Rust, but for logging to
/// the browser console instead of stdout.
#[macro_export]
macro_rules! console_dbg {
    ($val: expr) => {
        match $val {
            tmp => {
                console_log!(
                    "[{}:{}] {} = {:#?}",
                    file!(),
                    line!(),
                    stringify!($val),
                    &tmp
                );
                tmp
            }
        }
    };
}
