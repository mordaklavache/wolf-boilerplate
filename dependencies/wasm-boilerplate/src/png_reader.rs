//! # Image reader module
//!
//! This module provides a function to load PNG image files into
//! RGBA pixel buffers usable for texture mapping or pixel manipulation.
//!
//! It utilizes the 'png' crate to decode PNG data into a Vec of RGBA values.
//! The height/width of the image is also returned.

use crate::File;

use png::OutputInfo;

/// Decodes a PNG file from a byte slice into a pixel buffer.
///
/// Takes a byte slice containing the raw PNG file data. Uses the
/// png crate to decode this into a RGBA pixel buffer stored in a Vec.
pub fn get_image_buffer(png_slice: &[u8]) -> (Vec<u8>, OutputInfo) {
    let png_file = File::from_slice(png_slice);
    let decoder = png::Decoder::new(png_file);
    let mut reader = decoder.read_info().unwrap();
    let mut buf = vec![0; reader.output_buffer_size()];
    let info = reader.next_frame(&mut buf).unwrap();
    log::info!("{:?}", info);
    (buf, info)
}
