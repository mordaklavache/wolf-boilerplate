//! # Audio playback module
//!
//! This module provides a simple function to play audio clips from byte buffers.
//!
//! It uses the Web Audio API to decode and play audio data. Audio formats like
//! MP3, WAV, and OGG are supported.
//!
//! The main entry point is the `play_song()` function which takes a slice of bytes
//! containing the raw audio data, decodes it, and plays it back.
use wasm_bindgen::prelude::*;

use js_sys::{ArrayBuffer, Uint8Array};

/// Plays an audio clip from a byte slice containing audio data.
///
/// This function uses the Web Audio API to decode and play an audio
/// buffer contained in a byte slice.
///
/// The audio data should be in a supported format like MP3, WAV, OGG etc.
pub fn song(data: &[u8]) -> Result<(), JsValue> {
    let audio_context = web_sys::AudioContext::new()?;

    fn slice_to_array_buffer(slice: &[u8]) -> Result<ArrayBuffer, JsValue> {
        let uint8_array = Uint8Array::from(slice);
        Ok(uint8_array.buffer())
    }

    let out = audio_context.decode_audio_data(&slice_to_array_buffer(data).unwrap())?;

    let song_success = Closure::wrap(Box::new(move |value: JsValue| {
        let audio_buffer: web_sys::AudioBuffer = value.unchecked_into();
        log::info!("duration: {}", audio_buffer.duration());
        log::info!("channels: {}", audio_buffer.number_of_channels());
        log::info!("length: {}", audio_buffer.length());
        log::info!("sample_rate: {}", audio_buffer.sample_rate());

        // Création de l'AudioContext
        let audio_context = web_sys::AudioContext::new().unwrap();

        // Création d'un AudioBufferSourceNode
        let source_node = audio_context.create_buffer_source().unwrap();
        source_node.set_buffer(Some(&audio_buffer));
        source_node.set_loop(true);

        // Connexion du noeud source à la destination audio
        source_node
            .connect_with_audio_node(&audio_context.destination())
            .unwrap();

        // Lecture de l'audio
        source_node.start().unwrap();
    }) as Box<dyn FnMut(_)>);

    let song_error = Closure::wrap(Box::new(move |_value: JsValue| {
        log::error!("FAILED TO DECODE SONG!");
    }) as Box<dyn FnMut(_)>);

    let _new_promise = out.then2(&song_success, &song_error);

    song_success.forget();
    song_error.forget();
    Ok(())
}
