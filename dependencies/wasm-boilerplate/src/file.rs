use std::io::{Read, Result};

pub struct File<'a> {
    inner: &'a [u8],
    offset: usize,
}

impl<'a> File<'a> {
    pub fn from_slice(inner: &'a [u8]) -> Self {
        Self { inner, offset: 0 }
    }
}

impl<'a> Read for File<'a> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let mut len = buf.len();
        if len + self.offset > self.inner.len() {
            len = self.inner.len() - self.offset;
        }
        unsafe {
            std::ptr::copy(self.inner.as_ptr().add(self.offset), buf.as_mut_ptr(), len);
        }
        self.offset += len;
        Ok(len)
    }
}
